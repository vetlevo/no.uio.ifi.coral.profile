/**
 */
package Coral.CoralRiskMeasureAnnotations;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see Coral.CoralRiskMeasureAnnotations.CoralRiskMeasureAnnotationsPackage
 * @generated
 */
public interface CoralRiskMeasureAnnotationsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoralRiskMeasureAnnotationsFactory eINSTANCE = Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Frequency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Frequency</em>'.
	 * @generated
	 */
	Frequency createFrequency();

	/**
	 * Returns a new object of class '<em>Likelihood</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Likelihood</em>'.
	 * @generated
	 */
	Likelihood createLikelihood();

	/**
	 * Returns a new object of class '<em>Consequence</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Consequence</em>'.
	 * @generated
	 */
	Consequence createConsequence();

	/**
	 * Returns a new object of class '<em>Conditional Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conditional Ratio</em>'.
	 * @generated
	 */
	ConditionalRatio createConditionalRatio();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CoralRiskMeasureAnnotationsPackage getCoralRiskMeasureAnnotationsPackage();

} //CoralRiskMeasureAnnotationsFactory
