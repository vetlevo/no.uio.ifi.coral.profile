/**
 */
package Coral.CoralRiskMeasureAnnotations;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Coral.CoralRiskMeasureAnnotations.CoralRiskMeasureAnnotationsFactory
 * @model kind="package"
 * @generated
 */
public interface CoralRiskMeasureAnnotationsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "CoralRiskMeasureAnnotations";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///Coral/CoralRiskMeasureAnnotations.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Coral.CoralRiskMeasureAnnotations";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoralRiskMeasureAnnotationsPackage eINSTANCE = Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsPackageImpl.init();

	/**
	 * The meta object id for the '{@link Coral.CoralRiskMeasureAnnotations.impl.FrequencyImpl <em>Frequency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralRiskMeasureAnnotations.impl.FrequencyImpl
	 * @see Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsPackageImpl#getFrequency()
	 * @generated
	 */
	int FREQUENCY = 0;

	/**
	 * The feature id for the '<em><b>Base Comment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENCY__BASE_COMMENT = 0;

	/**
	 * The number of structural features of the '<em>Frequency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENCY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link Coral.CoralRiskMeasureAnnotations.impl.LikelihoodImpl <em>Likelihood</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralRiskMeasureAnnotations.impl.LikelihoodImpl
	 * @see Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsPackageImpl#getLikelihood()
	 * @generated
	 */
	int LIKELIHOOD = 1;

	/**
	 * The feature id for the '<em><b>Base Comment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIKELIHOOD__BASE_COMMENT = 0;

	/**
	 * The number of structural features of the '<em>Likelihood</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIKELIHOOD_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link Coral.CoralRiskMeasureAnnotations.impl.ConsequenceImpl <em>Consequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralRiskMeasureAnnotations.impl.ConsequenceImpl
	 * @see Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsPackageImpl#getConsequence()
	 * @generated
	 */
	int CONSEQUENCE = 2;

	/**
	 * The feature id for the '<em><b>Base Comment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSEQUENCE__BASE_COMMENT = 0;

	/**
	 * The number of structural features of the '<em>Consequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSEQUENCE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link Coral.CoralRiskMeasureAnnotations.impl.ConditionalRatioImpl <em>Conditional Ratio</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralRiskMeasureAnnotations.impl.ConditionalRatioImpl
	 * @see Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsPackageImpl#getConditionalRatio()
	 * @generated
	 */
	int CONDITIONAL_RATIO = 3;

	/**
	 * The feature id for the '<em><b>Base Comment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_RATIO__BASE_COMMENT = 0;

	/**
	 * The number of structural features of the '<em>Conditional Ratio</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_RATIO_FEATURE_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link Coral.CoralRiskMeasureAnnotations.Frequency <em>Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Frequency</em>'.
	 * @see Coral.CoralRiskMeasureAnnotations.Frequency
	 * @generated
	 */
	EClass getFrequency();

	/**
	 * Returns the meta object for the reference '{@link Coral.CoralRiskMeasureAnnotations.Frequency#getBase_Comment <em>Base Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Comment</em>'.
	 * @see Coral.CoralRiskMeasureAnnotations.Frequency#getBase_Comment()
	 * @see #getFrequency()
	 * @generated
	 */
	EReference getFrequency_Base_Comment();

	/**
	 * Returns the meta object for class '{@link Coral.CoralRiskMeasureAnnotations.Likelihood <em>Likelihood</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Likelihood</em>'.
	 * @see Coral.CoralRiskMeasureAnnotations.Likelihood
	 * @generated
	 */
	EClass getLikelihood();

	/**
	 * Returns the meta object for the reference '{@link Coral.CoralRiskMeasureAnnotations.Likelihood#getBase_Comment <em>Base Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Comment</em>'.
	 * @see Coral.CoralRiskMeasureAnnotations.Likelihood#getBase_Comment()
	 * @see #getLikelihood()
	 * @generated
	 */
	EReference getLikelihood_Base_Comment();

	/**
	 * Returns the meta object for class '{@link Coral.CoralRiskMeasureAnnotations.Consequence <em>Consequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Consequence</em>'.
	 * @see Coral.CoralRiskMeasureAnnotations.Consequence
	 * @generated
	 */
	EClass getConsequence();

	/**
	 * Returns the meta object for the reference '{@link Coral.CoralRiskMeasureAnnotations.Consequence#getBase_Comment <em>Base Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Comment</em>'.
	 * @see Coral.CoralRiskMeasureAnnotations.Consequence#getBase_Comment()
	 * @see #getConsequence()
	 * @generated
	 */
	EReference getConsequence_Base_Comment();

	/**
	 * Returns the meta object for class '{@link Coral.CoralRiskMeasureAnnotations.ConditionalRatio <em>Conditional Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conditional Ratio</em>'.
	 * @see Coral.CoralRiskMeasureAnnotations.ConditionalRatio
	 * @generated
	 */
	EClass getConditionalRatio();

	/**
	 * Returns the meta object for the reference '{@link Coral.CoralRiskMeasureAnnotations.ConditionalRatio#getBase_Comment <em>Base Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Comment</em>'.
	 * @see Coral.CoralRiskMeasureAnnotations.ConditionalRatio#getBase_Comment()
	 * @see #getConditionalRatio()
	 * @generated
	 */
	EReference getConditionalRatio_Base_Comment();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CoralRiskMeasureAnnotationsFactory getCoralRiskMeasureAnnotationsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Coral.CoralRiskMeasureAnnotations.impl.FrequencyImpl <em>Frequency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralRiskMeasureAnnotations.impl.FrequencyImpl
		 * @see Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsPackageImpl#getFrequency()
		 * @generated
		 */
		EClass FREQUENCY = eINSTANCE.getFrequency();

		/**
		 * The meta object literal for the '<em><b>Base Comment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FREQUENCY__BASE_COMMENT = eINSTANCE.getFrequency_Base_Comment();

		/**
		 * The meta object literal for the '{@link Coral.CoralRiskMeasureAnnotations.impl.LikelihoodImpl <em>Likelihood</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralRiskMeasureAnnotations.impl.LikelihoodImpl
		 * @see Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsPackageImpl#getLikelihood()
		 * @generated
		 */
		EClass LIKELIHOOD = eINSTANCE.getLikelihood();

		/**
		 * The meta object literal for the '<em><b>Base Comment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIKELIHOOD__BASE_COMMENT = eINSTANCE.getLikelihood_Base_Comment();

		/**
		 * The meta object literal for the '{@link Coral.CoralRiskMeasureAnnotations.impl.ConsequenceImpl <em>Consequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralRiskMeasureAnnotations.impl.ConsequenceImpl
		 * @see Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsPackageImpl#getConsequence()
		 * @generated
		 */
		EClass CONSEQUENCE = eINSTANCE.getConsequence();

		/**
		 * The meta object literal for the '<em><b>Base Comment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSEQUENCE__BASE_COMMENT = eINSTANCE.getConsequence_Base_Comment();

		/**
		 * The meta object literal for the '{@link Coral.CoralRiskMeasureAnnotations.impl.ConditionalRatioImpl <em>Conditional Ratio</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralRiskMeasureAnnotations.impl.ConditionalRatioImpl
		 * @see Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsPackageImpl#getConditionalRatio()
		 * @generated
		 */
		EClass CONDITIONAL_RATIO = eINSTANCE.getConditionalRatio();

		/**
		 * The meta object literal for the '<em><b>Base Comment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITIONAL_RATIO__BASE_COMMENT = eINSTANCE.getConditionalRatio_Base_Comment();

	}

} //CoralRiskMeasureAnnotationsPackage
