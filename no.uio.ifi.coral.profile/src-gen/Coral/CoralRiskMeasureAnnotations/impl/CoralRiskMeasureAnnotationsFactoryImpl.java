/**
 */
package Coral.CoralRiskMeasureAnnotations.impl;

import Coral.CoralRiskMeasureAnnotations.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoralRiskMeasureAnnotationsFactoryImpl extends EFactoryImpl implements CoralRiskMeasureAnnotationsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CoralRiskMeasureAnnotationsFactory init() {
		try {
			CoralRiskMeasureAnnotationsFactory theCoralRiskMeasureAnnotationsFactory = (CoralRiskMeasureAnnotationsFactory)EPackage.Registry.INSTANCE.getEFactory(CoralRiskMeasureAnnotationsPackage.eNS_URI);
			if (theCoralRiskMeasureAnnotationsFactory != null) {
				return theCoralRiskMeasureAnnotationsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CoralRiskMeasureAnnotationsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralRiskMeasureAnnotationsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CoralRiskMeasureAnnotationsPackage.FREQUENCY: return createFrequency();
			case CoralRiskMeasureAnnotationsPackage.LIKELIHOOD: return createLikelihood();
			case CoralRiskMeasureAnnotationsPackage.CONSEQUENCE: return createConsequence();
			case CoralRiskMeasureAnnotationsPackage.CONDITIONAL_RATIO: return createConditionalRatio();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Frequency createFrequency() {
		FrequencyImpl frequency = new FrequencyImpl();
		return frequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Likelihood createLikelihood() {
		LikelihoodImpl likelihood = new LikelihoodImpl();
		return likelihood;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Consequence createConsequence() {
		ConsequenceImpl consequence = new ConsequenceImpl();
		return consequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionalRatio createConditionalRatio() {
		ConditionalRatioImpl conditionalRatio = new ConditionalRatioImpl();
		return conditionalRatio;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralRiskMeasureAnnotationsPackage getCoralRiskMeasureAnnotationsPackage() {
		return (CoralRiskMeasureAnnotationsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static CoralRiskMeasureAnnotationsPackage getPackage() {
		return CoralRiskMeasureAnnotationsPackage.eINSTANCE;
	}

} //CoralRiskMeasureAnnotationsFactoryImpl
