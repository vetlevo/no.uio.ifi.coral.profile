/**
 */
package Coral.CoralRiskMeasureAnnotations.impl;

import Coral.CoralDataTypes.CoralDataTypesPackage;

import Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl;

import Coral.CoralLifelines.CoralLifelinesPackage;

import Coral.CoralLifelines.impl.CoralLifelinesPackageImpl;

import Coral.CoralMessages.CoralExactMessages.CoralExactMessagesPackage;

import Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl;

import Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage;

import Coral.CoralMessages.CoralIntervalMessages.impl.CoralIntervalMessagesPackageImpl;

import Coral.CoralMessages.CoralMessagesPackage;

import Coral.CoralMessages.impl.CoralMessagesPackageImpl;

import Coral.CoralRiskMeasureAnnotations.ConditionalRatio;
import Coral.CoralRiskMeasureAnnotations.Consequence;
import Coral.CoralRiskMeasureAnnotations.CoralRiskMeasureAnnotationsFactory;
import Coral.CoralRiskMeasureAnnotations.CoralRiskMeasureAnnotationsPackage;
import Coral.CoralRiskMeasureAnnotations.Frequency;
import Coral.CoralRiskMeasureAnnotations.Likelihood;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoralRiskMeasureAnnotationsPackageImpl extends EPackageImpl implements CoralRiskMeasureAnnotationsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass frequencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass likelihoodEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass consequenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionalRatioEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see Coral.CoralRiskMeasureAnnotations.CoralRiskMeasureAnnotationsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CoralRiskMeasureAnnotationsPackageImpl() {
		super(eNS_URI, CoralRiskMeasureAnnotationsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CoralRiskMeasureAnnotationsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CoralRiskMeasureAnnotationsPackage init() {
		if (isInited) return (CoralRiskMeasureAnnotationsPackage)EPackage.Registry.INSTANCE.getEPackage(CoralRiskMeasureAnnotationsPackage.eNS_URI);

		// Obtain or create and register package
		CoralRiskMeasureAnnotationsPackageImpl theCoralRiskMeasureAnnotationsPackage = (CoralRiskMeasureAnnotationsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CoralRiskMeasureAnnotationsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CoralRiskMeasureAnnotationsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CoralLifelinesPackageImpl theCoralLifelinesPackage = (CoralLifelinesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralLifelinesPackage.eNS_URI) instanceof CoralLifelinesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralLifelinesPackage.eNS_URI) : CoralLifelinesPackage.eINSTANCE);
		CoralDataTypesPackageImpl theCoralDataTypesPackage = (CoralDataTypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralDataTypesPackage.eNS_URI) instanceof CoralDataTypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralDataTypesPackage.eNS_URI) : CoralDataTypesPackage.eINSTANCE);
		CoralMessagesPackageImpl theCoralMessagesPackage = (CoralMessagesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralMessagesPackage.eNS_URI) instanceof CoralMessagesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralMessagesPackage.eNS_URI) : CoralMessagesPackage.eINSTANCE);
		CoralIntervalMessagesPackageImpl theCoralIntervalMessagesPackage = (CoralIntervalMessagesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralIntervalMessagesPackage.eNS_URI) instanceof CoralIntervalMessagesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralIntervalMessagesPackage.eNS_URI) : CoralIntervalMessagesPackage.eINSTANCE);
		CoralExactMessagesPackageImpl theCoralExactMessagesPackage = (CoralExactMessagesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralExactMessagesPackage.eNS_URI) instanceof CoralExactMessagesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralExactMessagesPackage.eNS_URI) : CoralExactMessagesPackage.eINSTANCE);

		// Create package meta-data objects
		theCoralRiskMeasureAnnotationsPackage.createPackageContents();
		theCoralLifelinesPackage.createPackageContents();
		theCoralDataTypesPackage.createPackageContents();
		theCoralMessagesPackage.createPackageContents();
		theCoralIntervalMessagesPackage.createPackageContents();
		theCoralExactMessagesPackage.createPackageContents();

		// Initialize created meta-data
		theCoralRiskMeasureAnnotationsPackage.initializePackageContents();
		theCoralLifelinesPackage.initializePackageContents();
		theCoralDataTypesPackage.initializePackageContents();
		theCoralMessagesPackage.initializePackageContents();
		theCoralIntervalMessagesPackage.initializePackageContents();
		theCoralExactMessagesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCoralRiskMeasureAnnotationsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CoralRiskMeasureAnnotationsPackage.eNS_URI, theCoralRiskMeasureAnnotationsPackage);
		return theCoralRiskMeasureAnnotationsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFrequency() {
		return frequencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFrequency_Base_Comment() {
		return (EReference)frequencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLikelihood() {
		return likelihoodEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLikelihood_Base_Comment() {
		return (EReference)likelihoodEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConsequence() {
		return consequenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConsequence_Base_Comment() {
		return (EReference)consequenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConditionalRatio() {
		return conditionalRatioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConditionalRatio_Base_Comment() {
		return (EReference)conditionalRatioEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralRiskMeasureAnnotationsFactory getCoralRiskMeasureAnnotationsFactory() {
		return (CoralRiskMeasureAnnotationsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		frequencyEClass = createEClass(FREQUENCY);
		createEReference(frequencyEClass, FREQUENCY__BASE_COMMENT);

		likelihoodEClass = createEClass(LIKELIHOOD);
		createEReference(likelihoodEClass, LIKELIHOOD__BASE_COMMENT);

		consequenceEClass = createEClass(CONSEQUENCE);
		createEReference(consequenceEClass, CONSEQUENCE__BASE_COMMENT);

		conditionalRatioEClass = createEClass(CONDITIONAL_RATIO);
		createEReference(conditionalRatioEClass, CONDITIONAL_RATIO__BASE_COMMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(frequencyEClass, Frequency.class, "Frequency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFrequency_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, Frequency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(likelihoodEClass, Likelihood.class, "Likelihood", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLikelihood_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, Likelihood.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(consequenceEClass, Consequence.class, "Consequence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConsequence_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, Consequence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(conditionalRatioEClass, ConditionalRatio.class, "ConditionalRatio", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConditionalRatio_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, ConditionalRatio.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //CoralRiskMeasureAnnotationsPackageImpl
