/**
 */
package Coral.CoralLifelines.impl;

import Coral.CoralDataTypes.CoralDataTypesPackage;

import Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl;

import Coral.CoralLifelines.AccidentalThreat;
import Coral.CoralLifelines.Asset;
import Coral.CoralLifelines.CoralLifelinesFactory;
import Coral.CoralLifelines.CoralLifelinesPackage;
import Coral.CoralLifelines.DeliberateThreat;
import Coral.CoralLifelines.NonHumanThreat;

import Coral.CoralMessages.CoralExactMessages.CoralExactMessagesPackage;

import Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl;

import Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage;

import Coral.CoralMessages.CoralIntervalMessages.impl.CoralIntervalMessagesPackageImpl;

import Coral.CoralMessages.CoralMessagesPackage;

import Coral.CoralMessages.impl.CoralMessagesPackageImpl;

import Coral.CoralRiskMeasureAnnotations.CoralRiskMeasureAnnotationsPackage;

import Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoralLifelinesPackageImpl extends EPackageImpl implements CoralLifelinesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deliberateThreatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accidentalThreatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nonHumanThreatEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see Coral.CoralLifelines.CoralLifelinesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CoralLifelinesPackageImpl() {
		super(eNS_URI, CoralLifelinesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CoralLifelinesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CoralLifelinesPackage init() {
		if (isInited) return (CoralLifelinesPackage)EPackage.Registry.INSTANCE.getEPackage(CoralLifelinesPackage.eNS_URI);

		// Obtain or create and register package
		CoralLifelinesPackageImpl theCoralLifelinesPackage = (CoralLifelinesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CoralLifelinesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CoralLifelinesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CoralDataTypesPackageImpl theCoralDataTypesPackage = (CoralDataTypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralDataTypesPackage.eNS_URI) instanceof CoralDataTypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralDataTypesPackage.eNS_URI) : CoralDataTypesPackage.eINSTANCE);
		CoralMessagesPackageImpl theCoralMessagesPackage = (CoralMessagesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralMessagesPackage.eNS_URI) instanceof CoralMessagesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralMessagesPackage.eNS_URI) : CoralMessagesPackage.eINSTANCE);
		CoralIntervalMessagesPackageImpl theCoralIntervalMessagesPackage = (CoralIntervalMessagesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralIntervalMessagesPackage.eNS_URI) instanceof CoralIntervalMessagesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralIntervalMessagesPackage.eNS_URI) : CoralIntervalMessagesPackage.eINSTANCE);
		CoralExactMessagesPackageImpl theCoralExactMessagesPackage = (CoralExactMessagesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralExactMessagesPackage.eNS_URI) instanceof CoralExactMessagesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralExactMessagesPackage.eNS_URI) : CoralExactMessagesPackage.eINSTANCE);
		CoralRiskMeasureAnnotationsPackageImpl theCoralRiskMeasureAnnotationsPackage = (CoralRiskMeasureAnnotationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralRiskMeasureAnnotationsPackage.eNS_URI) instanceof CoralRiskMeasureAnnotationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralRiskMeasureAnnotationsPackage.eNS_URI) : CoralRiskMeasureAnnotationsPackage.eINSTANCE);

		// Create package meta-data objects
		theCoralLifelinesPackage.createPackageContents();
		theCoralDataTypesPackage.createPackageContents();
		theCoralMessagesPackage.createPackageContents();
		theCoralIntervalMessagesPackage.createPackageContents();
		theCoralExactMessagesPackage.createPackageContents();
		theCoralRiskMeasureAnnotationsPackage.createPackageContents();

		// Initialize created meta-data
		theCoralLifelinesPackage.initializePackageContents();
		theCoralDataTypesPackage.initializePackageContents();
		theCoralMessagesPackage.initializePackageContents();
		theCoralIntervalMessagesPackage.initializePackageContents();
		theCoralExactMessagesPackage.initializePackageContents();
		theCoralRiskMeasureAnnotationsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCoralLifelinesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CoralLifelinesPackage.eNS_URI, theCoralLifelinesPackage);
		return theCoralLifelinesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAsset() {
		return assetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAsset_Base_Lifeline() {
		return (EReference)assetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeliberateThreat() {
		return deliberateThreatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeliberateThreat_Base_Lifeline() {
		return (EReference)deliberateThreatEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccidentalThreat() {
		return accidentalThreatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccidentalThreat_Base_Lifeline() {
		return (EReference)accidentalThreatEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNonHumanThreat() {
		return nonHumanThreatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNonHumanThreat_Base_Lifeline() {
		return (EReference)nonHumanThreatEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralLifelinesFactory getCoralLifelinesFactory() {
		return (CoralLifelinesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		assetEClass = createEClass(ASSET);
		createEReference(assetEClass, ASSET__BASE_LIFELINE);

		deliberateThreatEClass = createEClass(DELIBERATE_THREAT);
		createEReference(deliberateThreatEClass, DELIBERATE_THREAT__BASE_LIFELINE);

		accidentalThreatEClass = createEClass(ACCIDENTAL_THREAT);
		createEReference(accidentalThreatEClass, ACCIDENTAL_THREAT__BASE_LIFELINE);

		nonHumanThreatEClass = createEClass(NON_HUMAN_THREAT);
		createEReference(nonHumanThreatEClass, NON_HUMAN_THREAT__BASE_LIFELINE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(assetEClass, Asset.class, "Asset", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAsset_Base_Lifeline(), theUMLPackage.getLifeline(), null, "base_Lifeline", null, 1, 1, Asset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(deliberateThreatEClass, DeliberateThreat.class, "DeliberateThreat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDeliberateThreat_Base_Lifeline(), theUMLPackage.getLifeline(), null, "base_Lifeline", null, 1, 1, DeliberateThreat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(accidentalThreatEClass, AccidentalThreat.class, "AccidentalThreat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAccidentalThreat_Base_Lifeline(), theUMLPackage.getLifeline(), null, "base_Lifeline", null, 1, 1, AccidentalThreat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(nonHumanThreatEClass, NonHumanThreat.class, "NonHumanThreat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNonHumanThreat_Base_Lifeline(), theUMLPackage.getLifeline(), null, "base_Lifeline", null, 1, 1, NonHumanThreat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //CoralLifelinesPackageImpl
