/**
 */
package Coral.CoralLifelines.impl;

import Coral.CoralLifelines.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoralLifelinesFactoryImpl extends EFactoryImpl implements CoralLifelinesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CoralLifelinesFactory init() {
		try {
			CoralLifelinesFactory theCoralLifelinesFactory = (CoralLifelinesFactory)EPackage.Registry.INSTANCE.getEFactory(CoralLifelinesPackage.eNS_URI);
			if (theCoralLifelinesFactory != null) {
				return theCoralLifelinesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CoralLifelinesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralLifelinesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CoralLifelinesPackage.ASSET: return createAsset();
			case CoralLifelinesPackage.DELIBERATE_THREAT: return createDeliberateThreat();
			case CoralLifelinesPackage.ACCIDENTAL_THREAT: return createAccidentalThreat();
			case CoralLifelinesPackage.NON_HUMAN_THREAT: return createNonHumanThreat();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Asset createAsset() {
		AssetImpl asset = new AssetImpl();
		return asset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeliberateThreat createDeliberateThreat() {
		DeliberateThreatImpl deliberateThreat = new DeliberateThreatImpl();
		return deliberateThreat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccidentalThreat createAccidentalThreat() {
		AccidentalThreatImpl accidentalThreat = new AccidentalThreatImpl();
		return accidentalThreat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NonHumanThreat createNonHumanThreat() {
		NonHumanThreatImpl nonHumanThreat = new NonHumanThreatImpl();
		return nonHumanThreat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralLifelinesPackage getCoralLifelinesPackage() {
		return (CoralLifelinesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static CoralLifelinesPackage getPackage() {
		return CoralLifelinesPackage.eINSTANCE;
	}

} //CoralLifelinesFactoryImpl
