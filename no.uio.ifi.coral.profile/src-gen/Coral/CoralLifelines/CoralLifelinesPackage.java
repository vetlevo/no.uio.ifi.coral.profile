/**
 */
package Coral.CoralLifelines;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Coral.CoralLifelines.CoralLifelinesFactory
 * @model kind="package"
 * @generated
 */
public interface CoralLifelinesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "CoralLifelines";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///Coral/CoralLifelines.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Coral.CoralLifelines";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoralLifelinesPackage eINSTANCE = Coral.CoralLifelines.impl.CoralLifelinesPackageImpl.init();

	/**
	 * The meta object id for the '{@link Coral.CoralLifelines.impl.AssetImpl <em>Asset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralLifelines.impl.AssetImpl
	 * @see Coral.CoralLifelines.impl.CoralLifelinesPackageImpl#getAsset()
	 * @generated
	 */
	int ASSET = 0;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__BASE_LIFELINE = 0;

	/**
	 * The number of structural features of the '<em>Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link Coral.CoralLifelines.impl.DeliberateThreatImpl <em>Deliberate Threat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralLifelines.impl.DeliberateThreatImpl
	 * @see Coral.CoralLifelines.impl.CoralLifelinesPackageImpl#getDeliberateThreat()
	 * @generated
	 */
	int DELIBERATE_THREAT = 1;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELIBERATE_THREAT__BASE_LIFELINE = 0;

	/**
	 * The number of structural features of the '<em>Deliberate Threat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELIBERATE_THREAT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link Coral.CoralLifelines.impl.AccidentalThreatImpl <em>Accidental Threat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralLifelines.impl.AccidentalThreatImpl
	 * @see Coral.CoralLifelines.impl.CoralLifelinesPackageImpl#getAccidentalThreat()
	 * @generated
	 */
	int ACCIDENTAL_THREAT = 2;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCIDENTAL_THREAT__BASE_LIFELINE = 0;

	/**
	 * The number of structural features of the '<em>Accidental Threat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCIDENTAL_THREAT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link Coral.CoralLifelines.impl.NonHumanThreatImpl <em>Non Human Threat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralLifelines.impl.NonHumanThreatImpl
	 * @see Coral.CoralLifelines.impl.CoralLifelinesPackageImpl#getNonHumanThreat()
	 * @generated
	 */
	int NON_HUMAN_THREAT = 3;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_HUMAN_THREAT__BASE_LIFELINE = 0;

	/**
	 * The number of structural features of the '<em>Non Human Threat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_HUMAN_THREAT_FEATURE_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link Coral.CoralLifelines.Asset <em>Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset</em>'.
	 * @see Coral.CoralLifelines.Asset
	 * @generated
	 */
	EClass getAsset();

	/**
	 * Returns the meta object for the reference '{@link Coral.CoralLifelines.Asset#getBase_Lifeline <em>Base Lifeline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Lifeline</em>'.
	 * @see Coral.CoralLifelines.Asset#getBase_Lifeline()
	 * @see #getAsset()
	 * @generated
	 */
	EReference getAsset_Base_Lifeline();

	/**
	 * Returns the meta object for class '{@link Coral.CoralLifelines.DeliberateThreat <em>Deliberate Threat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deliberate Threat</em>'.
	 * @see Coral.CoralLifelines.DeliberateThreat
	 * @generated
	 */
	EClass getDeliberateThreat();

	/**
	 * Returns the meta object for the reference '{@link Coral.CoralLifelines.DeliberateThreat#getBase_Lifeline <em>Base Lifeline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Lifeline</em>'.
	 * @see Coral.CoralLifelines.DeliberateThreat#getBase_Lifeline()
	 * @see #getDeliberateThreat()
	 * @generated
	 */
	EReference getDeliberateThreat_Base_Lifeline();

	/**
	 * Returns the meta object for class '{@link Coral.CoralLifelines.AccidentalThreat <em>Accidental Threat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Accidental Threat</em>'.
	 * @see Coral.CoralLifelines.AccidentalThreat
	 * @generated
	 */
	EClass getAccidentalThreat();

	/**
	 * Returns the meta object for the reference '{@link Coral.CoralLifelines.AccidentalThreat#getBase_Lifeline <em>Base Lifeline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Lifeline</em>'.
	 * @see Coral.CoralLifelines.AccidentalThreat#getBase_Lifeline()
	 * @see #getAccidentalThreat()
	 * @generated
	 */
	EReference getAccidentalThreat_Base_Lifeline();

	/**
	 * Returns the meta object for class '{@link Coral.CoralLifelines.NonHumanThreat <em>Non Human Threat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Non Human Threat</em>'.
	 * @see Coral.CoralLifelines.NonHumanThreat
	 * @generated
	 */
	EClass getNonHumanThreat();

	/**
	 * Returns the meta object for the reference '{@link Coral.CoralLifelines.NonHumanThreat#getBase_Lifeline <em>Base Lifeline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Lifeline</em>'.
	 * @see Coral.CoralLifelines.NonHumanThreat#getBase_Lifeline()
	 * @see #getNonHumanThreat()
	 * @generated
	 */
	EReference getNonHumanThreat_Base_Lifeline();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CoralLifelinesFactory getCoralLifelinesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Coral.CoralLifelines.impl.AssetImpl <em>Asset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralLifelines.impl.AssetImpl
		 * @see Coral.CoralLifelines.impl.CoralLifelinesPackageImpl#getAsset()
		 * @generated
		 */
		EClass ASSET = eINSTANCE.getAsset();

		/**
		 * The meta object literal for the '<em><b>Base Lifeline</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET__BASE_LIFELINE = eINSTANCE.getAsset_Base_Lifeline();

		/**
		 * The meta object literal for the '{@link Coral.CoralLifelines.impl.DeliberateThreatImpl <em>Deliberate Threat</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralLifelines.impl.DeliberateThreatImpl
		 * @see Coral.CoralLifelines.impl.CoralLifelinesPackageImpl#getDeliberateThreat()
		 * @generated
		 */
		EClass DELIBERATE_THREAT = eINSTANCE.getDeliberateThreat();

		/**
		 * The meta object literal for the '<em><b>Base Lifeline</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DELIBERATE_THREAT__BASE_LIFELINE = eINSTANCE.getDeliberateThreat_Base_Lifeline();

		/**
		 * The meta object literal for the '{@link Coral.CoralLifelines.impl.AccidentalThreatImpl <em>Accidental Threat</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralLifelines.impl.AccidentalThreatImpl
		 * @see Coral.CoralLifelines.impl.CoralLifelinesPackageImpl#getAccidentalThreat()
		 * @generated
		 */
		EClass ACCIDENTAL_THREAT = eINSTANCE.getAccidentalThreat();

		/**
		 * The meta object literal for the '<em><b>Base Lifeline</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCIDENTAL_THREAT__BASE_LIFELINE = eINSTANCE.getAccidentalThreat_Base_Lifeline();

		/**
		 * The meta object literal for the '{@link Coral.CoralLifelines.impl.NonHumanThreatImpl <em>Non Human Threat</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralLifelines.impl.NonHumanThreatImpl
		 * @see Coral.CoralLifelines.impl.CoralLifelinesPackageImpl#getNonHumanThreat()
		 * @generated
		 */
		EClass NON_HUMAN_THREAT = eINSTANCE.getNonHumanThreat();

		/**
		 * The meta object literal for the '<em><b>Base Lifeline</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NON_HUMAN_THREAT__BASE_LIFELINE = eINSTANCE.getNonHumanThreat_Base_Lifeline();

	}

} //CoralLifelinesPackage
