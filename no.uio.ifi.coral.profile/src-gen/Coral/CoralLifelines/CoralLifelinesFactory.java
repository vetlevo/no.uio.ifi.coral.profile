/**
 */
package Coral.CoralLifelines;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see Coral.CoralLifelines.CoralLifelinesPackage
 * @generated
 */
public interface CoralLifelinesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoralLifelinesFactory eINSTANCE = Coral.CoralLifelines.impl.CoralLifelinesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Asset</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Asset</em>'.
	 * @generated
	 */
	Asset createAsset();

	/**
	 * Returns a new object of class '<em>Deliberate Threat</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deliberate Threat</em>'.
	 * @generated
	 */
	DeliberateThreat createDeliberateThreat();

	/**
	 * Returns a new object of class '<em>Accidental Threat</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Accidental Threat</em>'.
	 * @generated
	 */
	AccidentalThreat createAccidentalThreat();

	/**
	 * Returns a new object of class '<em>Non Human Threat</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Non Human Threat</em>'.
	 * @generated
	 */
	NonHumanThreat createNonHumanThreat();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CoralLifelinesPackage getCoralLifelinesPackage();

} //CoralLifelinesFactory
