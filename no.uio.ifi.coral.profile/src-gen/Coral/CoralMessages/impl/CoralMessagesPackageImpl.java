/**
 */
package Coral.CoralMessages.impl;

import Coral.CoralDataTypes.CoralDataTypesPackage;

import Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl;

import Coral.CoralLifelines.CoralLifelinesPackage;

import Coral.CoralLifelines.impl.CoralLifelinesPackageImpl;

import Coral.CoralMessages.CoralExactMessages.CoralExactMessagesPackage;

import Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl;

import Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage;

import Coral.CoralMessages.CoralIntervalMessages.impl.CoralIntervalMessagesPackageImpl;

import Coral.CoralMessages.CoralMessagesFactory;
import Coral.CoralMessages.CoralMessagesPackage;
import Coral.CoralMessages.DeletedMessage;

import Coral.CoralRiskMeasureAnnotations.CoralRiskMeasureAnnotationsPackage;

import Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoralMessagesPackageImpl extends EPackageImpl implements CoralMessagesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deletedMessageEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see Coral.CoralMessages.CoralMessagesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CoralMessagesPackageImpl() {
		super(eNS_URI, CoralMessagesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CoralMessagesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CoralMessagesPackage init() {
		if (isInited) return (CoralMessagesPackage)EPackage.Registry.INSTANCE.getEPackage(CoralMessagesPackage.eNS_URI);

		// Obtain or create and register package
		CoralMessagesPackageImpl theCoralMessagesPackage = (CoralMessagesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CoralMessagesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CoralMessagesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CoralLifelinesPackageImpl theCoralLifelinesPackage = (CoralLifelinesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralLifelinesPackage.eNS_URI) instanceof CoralLifelinesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralLifelinesPackage.eNS_URI) : CoralLifelinesPackage.eINSTANCE);
		CoralDataTypesPackageImpl theCoralDataTypesPackage = (CoralDataTypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralDataTypesPackage.eNS_URI) instanceof CoralDataTypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralDataTypesPackage.eNS_URI) : CoralDataTypesPackage.eINSTANCE);
		CoralIntervalMessagesPackageImpl theCoralIntervalMessagesPackage = (CoralIntervalMessagesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralIntervalMessagesPackage.eNS_URI) instanceof CoralIntervalMessagesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralIntervalMessagesPackage.eNS_URI) : CoralIntervalMessagesPackage.eINSTANCE);
		CoralExactMessagesPackageImpl theCoralExactMessagesPackage = (CoralExactMessagesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralExactMessagesPackage.eNS_URI) instanceof CoralExactMessagesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralExactMessagesPackage.eNS_URI) : CoralExactMessagesPackage.eINSTANCE);
		CoralRiskMeasureAnnotationsPackageImpl theCoralRiskMeasureAnnotationsPackage = (CoralRiskMeasureAnnotationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralRiskMeasureAnnotationsPackage.eNS_URI) instanceof CoralRiskMeasureAnnotationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralRiskMeasureAnnotationsPackage.eNS_URI) : CoralRiskMeasureAnnotationsPackage.eINSTANCE);

		// Create package meta-data objects
		theCoralMessagesPackage.createPackageContents();
		theCoralLifelinesPackage.createPackageContents();
		theCoralDataTypesPackage.createPackageContents();
		theCoralIntervalMessagesPackage.createPackageContents();
		theCoralExactMessagesPackage.createPackageContents();
		theCoralRiskMeasureAnnotationsPackage.createPackageContents();

		// Initialize created meta-data
		theCoralMessagesPackage.initializePackageContents();
		theCoralLifelinesPackage.initializePackageContents();
		theCoralDataTypesPackage.initializePackageContents();
		theCoralIntervalMessagesPackage.initializePackageContents();
		theCoralExactMessagesPackage.initializePackageContents();
		theCoralRiskMeasureAnnotationsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCoralMessagesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CoralMessagesPackage.eNS_URI, theCoralMessagesPackage);
		return theCoralMessagesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeletedMessage() {
		return deletedMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeletedMessage_Base_Message() {
		return (EReference)deletedMessageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralMessagesFactory getCoralMessagesFactory() {
		return (CoralMessagesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		deletedMessageEClass = createEClass(DELETED_MESSAGE);
		createEReference(deletedMessageEClass, DELETED_MESSAGE__BASE_MESSAGE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CoralIntervalMessagesPackage theCoralIntervalMessagesPackage = (CoralIntervalMessagesPackage)EPackage.Registry.INSTANCE.getEPackage(CoralIntervalMessagesPackage.eNS_URI);
		CoralExactMessagesPackage theCoralExactMessagesPackage = (CoralExactMessagesPackage)EPackage.Registry.INSTANCE.getEPackage(CoralExactMessagesPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theCoralIntervalMessagesPackage);
		getESubpackages().add(theCoralExactMessagesPackage);

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(deletedMessageEClass, DeletedMessage.class, "DeletedMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDeletedMessage_Base_Message(), theUMLPackage.getMessage(), null, "base_Message", null, 1, 1, DeletedMessage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //CoralMessagesPackageImpl
