/**
 */
package Coral.CoralMessages.impl;

import Coral.CoralMessages.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoralMessagesFactoryImpl extends EFactoryImpl implements CoralMessagesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CoralMessagesFactory init() {
		try {
			CoralMessagesFactory theCoralMessagesFactory = (CoralMessagesFactory)EPackage.Registry.INSTANCE.getEFactory(CoralMessagesPackage.eNS_URI);
			if (theCoralMessagesFactory != null) {
				return theCoralMessagesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CoralMessagesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralMessagesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CoralMessagesPackage.DELETED_MESSAGE: return createDeletedMessage();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeletedMessage createDeletedMessage() {
		DeletedMessageImpl deletedMessage = new DeletedMessageImpl();
		return deletedMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralMessagesPackage getCoralMessagesPackage() {
		return (CoralMessagesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static CoralMessagesPackage getPackage() {
		return CoralMessagesPackage.eINSTANCE;
	}

} //CoralMessagesFactoryImpl
