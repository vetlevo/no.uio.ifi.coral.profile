/**
 */
package Coral.CoralMessages.CoralIntervalMessages.util;

import Coral.CoralMessages.CoralIntervalMessages.*;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage
 * @generated
 */
public class CoralIntervalMessagesValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final CoralIntervalMessagesValidator INSTANCE = new CoralIntervalMessagesValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "Coral.CoralMessages.CoralIntervalMessages";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Recepient Is Asset' of 'Unwanted Incident'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int UNWANTED_INCIDENT__RECEPIENT_IS_ASSET = 1;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 1;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralIntervalMessagesValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EPackage getEPackage() {
	  return CoralIntervalMessagesPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map context) {
		switch (classifierID) {
			case CoralIntervalMessagesPackage.NEW_MESSAGE:
				return validateNewMessage((NewMessage)value, diagnostics, context);
			case CoralIntervalMessagesPackage.RISKY_MESSAGE:
				return validateRiskyMessage((RiskyMessage)value, diagnostics, context);
			case CoralIntervalMessagesPackage.ALTERED_MESSAGE:
				return validateAlteredMessage((AlteredMessage)value, diagnostics, context);
			case CoralIntervalMessagesPackage.GENERAL_MESSAGE:
				return validateGeneralMessage((GeneralMessage)value, diagnostics, context);
			case CoralIntervalMessagesPackage.UNWANTED_INCIDENT:
				return validateUnwantedIncident((UnwantedIncident)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNewMessage(NewMessage newMessage, DiagnosticChain diagnostics, Map context) {
		return validate_EveryDefaultConstraint(newMessage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRiskyMessage(RiskyMessage riskyMessage, DiagnosticChain diagnostics, Map context) {
		return validate_EveryDefaultConstraint(riskyMessage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAlteredMessage(AlteredMessage alteredMessage, DiagnosticChain diagnostics, Map context) {
		return validate_EveryDefaultConstraint(alteredMessage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGeneralMessage(GeneralMessage generalMessage, DiagnosticChain diagnostics, Map context) {
		return validate_EveryDefaultConstraint(generalMessage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUnwantedIncident(UnwantedIncident unwantedIncident, DiagnosticChain diagnostics, Map context) {
		boolean result = validate_EveryMultiplicityConforms(unwantedIncident, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(unwantedIncident, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(unwantedIncident, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(unwantedIncident, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(unwantedIncident, diagnostics, context);
		if (result || diagnostics != null) result &= validateUnwantedIncident_RecepientIsAsset(unwantedIncident, diagnostics, context);
		return result;
	}

	/**
	 * Validates the RecepientIsAsset constraint of '<em>Unwanted Incident</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUnwantedIncident_RecepientIsAsset(UnwantedIncident unwantedIncident, DiagnosticChain diagnostics, Map context) {
		return unwantedIncident.RecepientIsAsset(diagnostics, context);
	}

} //CoralIntervalMessagesValidator
