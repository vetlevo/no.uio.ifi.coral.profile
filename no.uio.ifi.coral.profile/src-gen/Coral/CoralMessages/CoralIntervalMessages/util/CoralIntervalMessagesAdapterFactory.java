/**
 */
package Coral.CoralMessages.CoralIntervalMessages.util;

import Coral.CoralMessages.CoralIntervalMessages.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage
 * @generated
 */
public class CoralIntervalMessagesAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CoralIntervalMessagesPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralIntervalMessagesAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = CoralIntervalMessagesPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CoralIntervalMessagesSwitch modelSwitch =
		new CoralIntervalMessagesSwitch() {
			public Object caseNewMessage(NewMessage object) {
				return createNewMessageAdapter();
			}
			public Object caseRiskyMessage(RiskyMessage object) {
				return createRiskyMessageAdapter();
			}
			public Object caseAlteredMessage(AlteredMessage object) {
				return createAlteredMessageAdapter();
			}
			public Object caseGeneralMessage(GeneralMessage object) {
				return createGeneralMessageAdapter();
			}
			public Object caseUnwantedIncident(UnwantedIncident object) {
				return createUnwantedIncidentAdapter();
			}
			public Object defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	public Adapter createAdapter(Notifier target) {
		return (Adapter)modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link Coral.CoralMessages.CoralIntervalMessages.NewMessage <em>New Message</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Coral.CoralMessages.CoralIntervalMessages.NewMessage
	 * @generated
	 */
	public Adapter createNewMessageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Coral.CoralMessages.CoralIntervalMessages.RiskyMessage <em>Risky Message</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Coral.CoralMessages.CoralIntervalMessages.RiskyMessage
	 * @generated
	 */
	public Adapter createRiskyMessageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Coral.CoralMessages.CoralIntervalMessages.AlteredMessage <em>Altered Message</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Coral.CoralMessages.CoralIntervalMessages.AlteredMessage
	 * @generated
	 */
	public Adapter createAlteredMessageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Coral.CoralMessages.CoralIntervalMessages.GeneralMessage <em>General Message</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Coral.CoralMessages.CoralIntervalMessages.GeneralMessage
	 * @generated
	 */
	public Adapter createGeneralMessageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Coral.CoralMessages.CoralIntervalMessages.UnwantedIncident <em>Unwanted Incident</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Coral.CoralMessages.CoralIntervalMessages.UnwantedIncident
	 * @generated
	 */
	public Adapter createUnwantedIncidentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //CoralIntervalMessagesAdapterFactory
