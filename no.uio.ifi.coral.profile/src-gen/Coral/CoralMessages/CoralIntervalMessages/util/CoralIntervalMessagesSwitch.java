/**
 */
package Coral.CoralMessages.CoralIntervalMessages.util;

import Coral.CoralMessages.CoralIntervalMessages.*;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage
 * @generated
 */
public class CoralIntervalMessagesSwitch {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CoralIntervalMessagesPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralIntervalMessagesSwitch() {
		if (modelPackage == null) {
			modelPackage = CoralIntervalMessagesPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public Object doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch((EClass)eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case CoralIntervalMessagesPackage.NEW_MESSAGE: {
				NewMessage newMessage = (NewMessage)theEObject;
				Object result = caseNewMessage(newMessage);
				if (result == null) result = caseRiskyMessage(newMessage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CoralIntervalMessagesPackage.RISKY_MESSAGE: {
				RiskyMessage riskyMessage = (RiskyMessage)theEObject;
				Object result = caseRiskyMessage(riskyMessage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CoralIntervalMessagesPackage.ALTERED_MESSAGE: {
				AlteredMessage alteredMessage = (AlteredMessage)theEObject;
				Object result = caseAlteredMessage(alteredMessage);
				if (result == null) result = caseRiskyMessage(alteredMessage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CoralIntervalMessagesPackage.GENERAL_MESSAGE: {
				GeneralMessage generalMessage = (GeneralMessage)theEObject;
				Object result = caseGeneralMessage(generalMessage);
				if (result == null) result = caseRiskyMessage(generalMessage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CoralIntervalMessagesPackage.UNWANTED_INCIDENT: {
				UnwantedIncident unwantedIncident = (UnwantedIncident)theEObject;
				Object result = caseUnwantedIncident(unwantedIncident);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>New Message</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>New Message</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseNewMessage(NewMessage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Risky Message</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Risky Message</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRiskyMessage(RiskyMessage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Altered Message</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Altered Message</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAlteredMessage(AlteredMessage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>General Message</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>General Message</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseGeneralMessage(GeneralMessage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unwanted Incident</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unwanted Incident</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseUnwantedIncident(UnwantedIncident object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public Object defaultCase(EObject object) {
		return null;
	}

} //CoralIntervalMessagesSwitch
