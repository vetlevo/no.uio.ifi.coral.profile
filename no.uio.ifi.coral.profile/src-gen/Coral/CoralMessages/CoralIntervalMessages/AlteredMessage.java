/**
 */
package Coral.CoralMessages.CoralIntervalMessages;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Altered Message</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage#getAlteredMessage()
 * @model
 * @generated
 */
public interface AlteredMessage extends RiskyMessage {
} // AlteredMessage
