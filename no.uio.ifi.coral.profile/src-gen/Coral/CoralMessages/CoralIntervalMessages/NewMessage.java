/**
 */
package Coral.CoralMessages.CoralIntervalMessages;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>New Message</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage#getNewMessage()
 * @model
 * @generated
 */
public interface NewMessage extends RiskyMessage {
} // NewMessage
