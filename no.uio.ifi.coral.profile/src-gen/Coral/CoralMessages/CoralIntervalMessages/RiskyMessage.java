/**
 */
package Coral.CoralMessages.CoralIntervalMessages;

import Coral.CoralDataTypes.IntCondRatio;
import Coral.CoralDataTypes.IntFrequency;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Message;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Risky Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Coral.CoralMessages.CoralIntervalMessages.RiskyMessage#getBase_Message <em>Base Message</em>}</li>
 *   <li>{@link Coral.CoralMessages.CoralIntervalMessages.RiskyMessage#getTransmissionFrequency <em>Transmission Frequency</em>}</li>
 *   <li>{@link Coral.CoralMessages.CoralIntervalMessages.RiskyMessage#getReceptionFrequency <em>Reception Frequency</em>}</li>
 *   <li>{@link Coral.CoralMessages.CoralIntervalMessages.RiskyMessage#getConditionalRatio <em>Conditional Ratio</em>}</li>
 * </ul>
 *
 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage#getRiskyMessage()
 * @model abstract="true"
 * @generated
 */
public interface RiskyMessage extends EObject {
	/**
	 * Returns the value of the '<em><b>Base Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Message</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Message</em>' reference.
	 * @see #setBase_Message(Message)
	 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage#getRiskyMessage_Base_Message()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Message getBase_Message();

	/**
	 * Sets the value of the '{@link Coral.CoralMessages.CoralIntervalMessages.RiskyMessage#getBase_Message <em>Base Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Message</em>' reference.
	 * @see #getBase_Message()
	 * @generated
	 */
	void setBase_Message(Message value);

	/**
	 * Returns the value of the '<em><b>Transmission Frequency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transmission Frequency</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transmission Frequency</em>' containment reference.
	 * @see #setTransmissionFrequency(IntFrequency)
	 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage#getRiskyMessage_TransmissionFrequency()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	IntFrequency getTransmissionFrequency();

	/**
	 * Sets the value of the '{@link Coral.CoralMessages.CoralIntervalMessages.RiskyMessage#getTransmissionFrequency <em>Transmission Frequency</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transmission Frequency</em>' containment reference.
	 * @see #getTransmissionFrequency()
	 * @generated
	 */
	void setTransmissionFrequency(IntFrequency value);

	/**
	 * Returns the value of the '<em><b>Reception Frequency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reception Frequency</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reception Frequency</em>' containment reference.
	 * @see #setReceptionFrequency(IntFrequency)
	 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage#getRiskyMessage_ReceptionFrequency()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	IntFrequency getReceptionFrequency();

	/**
	 * Sets the value of the '{@link Coral.CoralMessages.CoralIntervalMessages.RiskyMessage#getReceptionFrequency <em>Reception Frequency</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reception Frequency</em>' containment reference.
	 * @see #getReceptionFrequency()
	 * @generated
	 */
	void setReceptionFrequency(IntFrequency value);

	/**
	 * Returns the value of the '<em><b>Conditional Ratio</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conditional Ratio</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conditional Ratio</em>' containment reference.
	 * @see #setConditionalRatio(IntCondRatio)
	 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage#getRiskyMessage_ConditionalRatio()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	IntCondRatio getConditionalRatio();

	/**
	 * Sets the value of the '{@link Coral.CoralMessages.CoralIntervalMessages.RiskyMessage#getConditionalRatio <em>Conditional Ratio</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conditional Ratio</em>' containment reference.
	 * @see #getConditionalRatio()
	 * @generated
	 */
	void setConditionalRatio(IntCondRatio value);

} // RiskyMessage
