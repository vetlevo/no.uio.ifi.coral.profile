/**
 */
package Coral.CoralMessages.CoralIntervalMessages.impl;

import Coral.CoralMessages.CoralIntervalMessages.AlteredMessage;
import Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Altered Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AlteredMessageImpl extends RiskyMessageImpl implements AlteredMessage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlteredMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CoralIntervalMessagesPackage.Literals.ALTERED_MESSAGE;
	}

} //AlteredMessageImpl
