/**
 */
package Coral.CoralMessages.CoralIntervalMessages.impl;

import Coral.CoralDataTypes.IntCondRatio;
import Coral.CoralDataTypes.IntFrequency;

import Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage;
import Coral.CoralMessages.CoralIntervalMessages.RiskyMessage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.uml2.uml.Message;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Risky Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Coral.CoralMessages.CoralIntervalMessages.impl.RiskyMessageImpl#getBase_Message <em>Base Message</em>}</li>
 *   <li>{@link Coral.CoralMessages.CoralIntervalMessages.impl.RiskyMessageImpl#getTransmissionFrequency <em>Transmission Frequency</em>}</li>
 *   <li>{@link Coral.CoralMessages.CoralIntervalMessages.impl.RiskyMessageImpl#getReceptionFrequency <em>Reception Frequency</em>}</li>
 *   <li>{@link Coral.CoralMessages.CoralIntervalMessages.impl.RiskyMessageImpl#getConditionalRatio <em>Conditional Ratio</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class RiskyMessageImpl extends MinimalEObjectImpl.Container implements RiskyMessage {
	/**
	 * The cached value of the '{@link #getBase_Message() <em>Base Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Message()
	 * @generated
	 * @ordered
	 */
	protected Message base_Message;

	/**
	 * The cached value of the '{@link #getTransmissionFrequency() <em>Transmission Frequency</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransmissionFrequency()
	 * @generated
	 * @ordered
	 */
	protected IntFrequency transmissionFrequency;

	/**
	 * The cached value of the '{@link #getReceptionFrequency() <em>Reception Frequency</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceptionFrequency()
	 * @generated
	 * @ordered
	 */
	protected IntFrequency receptionFrequency;

	/**
	 * The cached value of the '{@link #getConditionalRatio() <em>Conditional Ratio</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionalRatio()
	 * @generated
	 * @ordered
	 */
	protected IntCondRatio conditionalRatio;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RiskyMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CoralIntervalMessagesPackage.Literals.RISKY_MESSAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getBase_Message() {
		if (base_Message != null && base_Message.eIsProxy()) {
			InternalEObject oldBase_Message = (InternalEObject)base_Message;
			base_Message = (Message)eResolveProxy(oldBase_Message);
			if (base_Message != oldBase_Message) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoralIntervalMessagesPackage.RISKY_MESSAGE__BASE_MESSAGE, oldBase_Message, base_Message));
			}
		}
		return base_Message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message basicGetBase_Message() {
		return base_Message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Message(Message newBase_Message) {
		Message oldBase_Message = base_Message;
		base_Message = newBase_Message;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoralIntervalMessagesPackage.RISKY_MESSAGE__BASE_MESSAGE, oldBase_Message, base_Message));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntFrequency getTransmissionFrequency() {
		return transmissionFrequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTransmissionFrequency(IntFrequency newTransmissionFrequency, NotificationChain msgs) {
		IntFrequency oldTransmissionFrequency = transmissionFrequency;
		transmissionFrequency = newTransmissionFrequency;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoralIntervalMessagesPackage.RISKY_MESSAGE__TRANSMISSION_FREQUENCY, oldTransmissionFrequency, newTransmissionFrequency);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransmissionFrequency(IntFrequency newTransmissionFrequency) {
		if (newTransmissionFrequency != transmissionFrequency) {
			NotificationChain msgs = null;
			if (transmissionFrequency != null)
				msgs = ((InternalEObject)transmissionFrequency).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoralIntervalMessagesPackage.RISKY_MESSAGE__TRANSMISSION_FREQUENCY, null, msgs);
			if (newTransmissionFrequency != null)
				msgs = ((InternalEObject)newTransmissionFrequency).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CoralIntervalMessagesPackage.RISKY_MESSAGE__TRANSMISSION_FREQUENCY, null, msgs);
			msgs = basicSetTransmissionFrequency(newTransmissionFrequency, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoralIntervalMessagesPackage.RISKY_MESSAGE__TRANSMISSION_FREQUENCY, newTransmissionFrequency, newTransmissionFrequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntFrequency getReceptionFrequency() {
		return receptionFrequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReceptionFrequency(IntFrequency newReceptionFrequency, NotificationChain msgs) {
		IntFrequency oldReceptionFrequency = receptionFrequency;
		receptionFrequency = newReceptionFrequency;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoralIntervalMessagesPackage.RISKY_MESSAGE__RECEPTION_FREQUENCY, oldReceptionFrequency, newReceptionFrequency);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReceptionFrequency(IntFrequency newReceptionFrequency) {
		if (newReceptionFrequency != receptionFrequency) {
			NotificationChain msgs = null;
			if (receptionFrequency != null)
				msgs = ((InternalEObject)receptionFrequency).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoralIntervalMessagesPackage.RISKY_MESSAGE__RECEPTION_FREQUENCY, null, msgs);
			if (newReceptionFrequency != null)
				msgs = ((InternalEObject)newReceptionFrequency).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CoralIntervalMessagesPackage.RISKY_MESSAGE__RECEPTION_FREQUENCY, null, msgs);
			msgs = basicSetReceptionFrequency(newReceptionFrequency, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoralIntervalMessagesPackage.RISKY_MESSAGE__RECEPTION_FREQUENCY, newReceptionFrequency, newReceptionFrequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntCondRatio getConditionalRatio() {
		return conditionalRatio;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConditionalRatio(IntCondRatio newConditionalRatio, NotificationChain msgs) {
		IntCondRatio oldConditionalRatio = conditionalRatio;
		conditionalRatio = newConditionalRatio;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoralIntervalMessagesPackage.RISKY_MESSAGE__CONDITIONAL_RATIO, oldConditionalRatio, newConditionalRatio);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConditionalRatio(IntCondRatio newConditionalRatio) {
		if (newConditionalRatio != conditionalRatio) {
			NotificationChain msgs = null;
			if (conditionalRatio != null)
				msgs = ((InternalEObject)conditionalRatio).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoralIntervalMessagesPackage.RISKY_MESSAGE__CONDITIONAL_RATIO, null, msgs);
			if (newConditionalRatio != null)
				msgs = ((InternalEObject)newConditionalRatio).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CoralIntervalMessagesPackage.RISKY_MESSAGE__CONDITIONAL_RATIO, null, msgs);
			msgs = basicSetConditionalRatio(newConditionalRatio, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoralIntervalMessagesPackage.RISKY_MESSAGE__CONDITIONAL_RATIO, newConditionalRatio, newConditionalRatio));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__TRANSMISSION_FREQUENCY:
				return basicSetTransmissionFrequency(null, msgs);
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__RECEPTION_FREQUENCY:
				return basicSetReceptionFrequency(null, msgs);
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__CONDITIONAL_RATIO:
				return basicSetConditionalRatio(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__BASE_MESSAGE:
				if (resolve) return getBase_Message();
				return basicGetBase_Message();
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__TRANSMISSION_FREQUENCY:
				return getTransmissionFrequency();
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__RECEPTION_FREQUENCY:
				return getReceptionFrequency();
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__CONDITIONAL_RATIO:
				return getConditionalRatio();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__BASE_MESSAGE:
				setBase_Message((Message)newValue);
				return;
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__TRANSMISSION_FREQUENCY:
				setTransmissionFrequency((IntFrequency)newValue);
				return;
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__RECEPTION_FREQUENCY:
				setReceptionFrequency((IntFrequency)newValue);
				return;
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__CONDITIONAL_RATIO:
				setConditionalRatio((IntCondRatio)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__BASE_MESSAGE:
				setBase_Message((Message)null);
				return;
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__TRANSMISSION_FREQUENCY:
				setTransmissionFrequency((IntFrequency)null);
				return;
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__RECEPTION_FREQUENCY:
				setReceptionFrequency((IntFrequency)null);
				return;
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__CONDITIONAL_RATIO:
				setConditionalRatio((IntCondRatio)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__BASE_MESSAGE:
				return base_Message != null;
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__TRANSMISSION_FREQUENCY:
				return transmissionFrequency != null;
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__RECEPTION_FREQUENCY:
				return receptionFrequency != null;
			case CoralIntervalMessagesPackage.RISKY_MESSAGE__CONDITIONAL_RATIO:
				return conditionalRatio != null;
		}
		return super.eIsSet(featureID);
	}

} //RiskyMessageImpl
