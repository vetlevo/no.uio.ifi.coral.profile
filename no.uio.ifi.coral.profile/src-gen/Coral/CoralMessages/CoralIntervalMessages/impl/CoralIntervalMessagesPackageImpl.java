/**
 */
package Coral.CoralMessages.CoralIntervalMessages.impl;

import Coral.CoralDataTypes.CoralDataTypesPackage;

import Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl;

import Coral.CoralLifelines.CoralLifelinesPackage;

import Coral.CoralLifelines.impl.CoralLifelinesPackageImpl;

import Coral.CoralMessages.CoralExactMessages.CoralExactMessagesPackage;

import Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl;

import Coral.CoralMessages.CoralIntervalMessages.AlteredMessage;
import Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesFactory;
import Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage;
import Coral.CoralMessages.CoralIntervalMessages.GeneralMessage;
import Coral.CoralMessages.CoralIntervalMessages.NewMessage;
import Coral.CoralMessages.CoralIntervalMessages.RiskyMessage;
import Coral.CoralMessages.CoralIntervalMessages.UnwantedIncident;

import Coral.CoralMessages.CoralIntervalMessages.util.CoralIntervalMessagesValidator;

import Coral.CoralMessages.CoralMessagesPackage;

import Coral.CoralMessages.impl.CoralMessagesPackageImpl;

import Coral.CoralRiskMeasureAnnotations.CoralRiskMeasureAnnotationsPackage;

import Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoralIntervalMessagesPackageImpl extends EPackageImpl implements CoralIntervalMessagesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass newMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass riskyMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass alteredMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass generalMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unwantedIncidentEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CoralIntervalMessagesPackageImpl() {
		super(eNS_URI, CoralIntervalMessagesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CoralIntervalMessagesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CoralIntervalMessagesPackage init() {
		if (isInited) return (CoralIntervalMessagesPackage)EPackage.Registry.INSTANCE.getEPackage(CoralIntervalMessagesPackage.eNS_URI);

		// Obtain or create and register package
		CoralIntervalMessagesPackageImpl theCoralIntervalMessagesPackage = (CoralIntervalMessagesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CoralIntervalMessagesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CoralIntervalMessagesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CoralLifelinesPackageImpl theCoralLifelinesPackage = (CoralLifelinesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralLifelinesPackage.eNS_URI) instanceof CoralLifelinesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralLifelinesPackage.eNS_URI) : CoralLifelinesPackage.eINSTANCE);
		CoralDataTypesPackageImpl theCoralDataTypesPackage = (CoralDataTypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralDataTypesPackage.eNS_URI) instanceof CoralDataTypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralDataTypesPackage.eNS_URI) : CoralDataTypesPackage.eINSTANCE);
		CoralMessagesPackageImpl theCoralMessagesPackage = (CoralMessagesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralMessagesPackage.eNS_URI) instanceof CoralMessagesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralMessagesPackage.eNS_URI) : CoralMessagesPackage.eINSTANCE);
		CoralExactMessagesPackageImpl theCoralExactMessagesPackage = (CoralExactMessagesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralExactMessagesPackage.eNS_URI) instanceof CoralExactMessagesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralExactMessagesPackage.eNS_URI) : CoralExactMessagesPackage.eINSTANCE);
		CoralRiskMeasureAnnotationsPackageImpl theCoralRiskMeasureAnnotationsPackage = (CoralRiskMeasureAnnotationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralRiskMeasureAnnotationsPackage.eNS_URI) instanceof CoralRiskMeasureAnnotationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralRiskMeasureAnnotationsPackage.eNS_URI) : CoralRiskMeasureAnnotationsPackage.eINSTANCE);

		// Create package meta-data objects
		theCoralIntervalMessagesPackage.createPackageContents();
		theCoralLifelinesPackage.createPackageContents();
		theCoralDataTypesPackage.createPackageContents();
		theCoralMessagesPackage.createPackageContents();
		theCoralExactMessagesPackage.createPackageContents();
		theCoralRiskMeasureAnnotationsPackage.createPackageContents();

		// Initialize created meta-data
		theCoralIntervalMessagesPackage.initializePackageContents();
		theCoralLifelinesPackage.initializePackageContents();
		theCoralDataTypesPackage.initializePackageContents();
		theCoralMessagesPackage.initializePackageContents();
		theCoralExactMessagesPackage.initializePackageContents();
		theCoralRiskMeasureAnnotationsPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theCoralIntervalMessagesPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return CoralIntervalMessagesValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theCoralIntervalMessagesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CoralIntervalMessagesPackage.eNS_URI, theCoralIntervalMessagesPackage);
		return theCoralIntervalMessagesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNewMessage() {
		return newMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRiskyMessage() {
		return riskyMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRiskyMessage_Base_Message() {
		return (EReference)riskyMessageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRiskyMessage_TransmissionFrequency() {
		return (EReference)riskyMessageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRiskyMessage_ReceptionFrequency() {
		return (EReference)riskyMessageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRiskyMessage_ConditionalRatio() {
		return (EReference)riskyMessageEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlteredMessage() {
		return alteredMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGeneralMessage() {
		return generalMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnwantedIncident() {
		return unwantedIncidentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUnwantedIncident_Base_Message() {
		return (EReference)unwantedIncidentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUnwantedIncident_TransmissionFrequency() {
		return (EReference)unwantedIncidentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnwantedIncident_Consequence() {
		return (EAttribute)unwantedIncidentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralIntervalMessagesFactory getCoralIntervalMessagesFactory() {
		return (CoralIntervalMessagesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		newMessageEClass = createEClass(NEW_MESSAGE);

		riskyMessageEClass = createEClass(RISKY_MESSAGE);
		createEReference(riskyMessageEClass, RISKY_MESSAGE__BASE_MESSAGE);
		createEReference(riskyMessageEClass, RISKY_MESSAGE__TRANSMISSION_FREQUENCY);
		createEReference(riskyMessageEClass, RISKY_MESSAGE__RECEPTION_FREQUENCY);
		createEReference(riskyMessageEClass, RISKY_MESSAGE__CONDITIONAL_RATIO);

		alteredMessageEClass = createEClass(ALTERED_MESSAGE);

		generalMessageEClass = createEClass(GENERAL_MESSAGE);

		unwantedIncidentEClass = createEClass(UNWANTED_INCIDENT);
		createEReference(unwantedIncidentEClass, UNWANTED_INCIDENT__BASE_MESSAGE);
		createEReference(unwantedIncidentEClass, UNWANTED_INCIDENT__TRANSMISSION_FREQUENCY);
		createEAttribute(unwantedIncidentEClass, UNWANTED_INCIDENT__CONSEQUENCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		CoralDataTypesPackage theCoralDataTypesPackage = (CoralDataTypesPackage)EPackage.Registry.INSTANCE.getEPackage(CoralDataTypesPackage.eNS_URI);

		// Add supertypes to classes
		newMessageEClass.getESuperTypes().add(this.getRiskyMessage());
		alteredMessageEClass.getESuperTypes().add(this.getRiskyMessage());
		generalMessageEClass.getESuperTypes().add(this.getRiskyMessage());

		// Initialize classes and features; add operations and parameters
		initEClass(newMessageEClass, NewMessage.class, "NewMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(riskyMessageEClass, RiskyMessage.class, "RiskyMessage", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRiskyMessage_Base_Message(), theUMLPackage.getMessage(), null, "base_Message", null, 1, 1, RiskyMessage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRiskyMessage_TransmissionFrequency(), theCoralDataTypesPackage.getIntFrequency(), null, "transmissionFrequency", null, 1, 1, RiskyMessage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRiskyMessage_ReceptionFrequency(), theCoralDataTypesPackage.getIntFrequency(), null, "receptionFrequency", null, 1, 1, RiskyMessage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRiskyMessage_ConditionalRatio(), theCoralDataTypesPackage.getIntCondRatio(), null, "conditionalRatio", null, 1, 1, RiskyMessage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(alteredMessageEClass, AlteredMessage.class, "AlteredMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(generalMessageEClass, GeneralMessage.class, "GeneralMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(unwantedIncidentEClass, UnwantedIncident.class, "UnwantedIncident", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnwantedIncident_Base_Message(), theUMLPackage.getMessage(), null, "base_Message", null, 1, 1, UnwantedIncident.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getUnwantedIncident_TransmissionFrequency(), theCoralDataTypesPackage.getIntFrequency(), null, "transmissionFrequency", null, 1, 1, UnwantedIncident.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getUnwantedIncident_Consequence(), theCoralDataTypesPackage.getConsequences(), "consequence", null, 1, 1, UnwantedIncident.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		EOperation op = addEOperation(unwantedIncidentEClass, ecorePackage.getEBoolean(), "RecepientIsAsset", 0, 1);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1);
		addEParameter(op, ecorePackage.getEMap(), "context", 0, 1);
	}

} //CoralIntervalMessagesPackageImpl
