/**
 */
package Coral.CoralMessages.CoralIntervalMessages.impl;

import Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage;
import Coral.CoralMessages.CoralIntervalMessages.NewMessage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>New Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NewMessageImpl extends RiskyMessageImpl implements NewMessage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NewMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CoralIntervalMessagesPackage.Literals.NEW_MESSAGE;
	}

} //NewMessageImpl
