/**
 */
package Coral.CoralMessages.CoralIntervalMessages.impl;

import Coral.CoralMessages.CoralIntervalMessages.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoralIntervalMessagesFactoryImpl extends EFactoryImpl implements CoralIntervalMessagesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CoralIntervalMessagesFactory init() {
		try {
			CoralIntervalMessagesFactory theCoralIntervalMessagesFactory = (CoralIntervalMessagesFactory)EPackage.Registry.INSTANCE.getEFactory(CoralIntervalMessagesPackage.eNS_URI);
			if (theCoralIntervalMessagesFactory != null) {
				return theCoralIntervalMessagesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CoralIntervalMessagesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralIntervalMessagesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CoralIntervalMessagesPackage.NEW_MESSAGE: return createNewMessage();
			case CoralIntervalMessagesPackage.ALTERED_MESSAGE: return createAlteredMessage();
			case CoralIntervalMessagesPackage.GENERAL_MESSAGE: return createGeneralMessage();
			case CoralIntervalMessagesPackage.UNWANTED_INCIDENT: return createUnwantedIncident();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NewMessage createNewMessage() {
		NewMessageImpl newMessage = new NewMessageImpl();
		return newMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlteredMessage createAlteredMessage() {
		AlteredMessageImpl alteredMessage = new AlteredMessageImpl();
		return alteredMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneralMessage createGeneralMessage() {
		GeneralMessageImpl generalMessage = new GeneralMessageImpl();
		return generalMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnwantedIncident createUnwantedIncident() {
		UnwantedIncidentImpl unwantedIncident = new UnwantedIncidentImpl();
		return unwantedIncident;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralIntervalMessagesPackage getCoralIntervalMessagesPackage() {
		return (CoralIntervalMessagesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static CoralIntervalMessagesPackage getPackage() {
		return CoralIntervalMessagesPackage.eINSTANCE;
	}

} //CoralIntervalMessagesFactoryImpl
