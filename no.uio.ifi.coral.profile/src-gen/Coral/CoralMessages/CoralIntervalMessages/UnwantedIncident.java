/**
 */
package Coral.CoralMessages.CoralIntervalMessages;

import Coral.CoralDataTypes.Consequences;
import Coral.CoralDataTypes.IntFrequency;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Message;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unwanted Incident</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Coral.CoralMessages.CoralIntervalMessages.UnwantedIncident#getBase_Message <em>Base Message</em>}</li>
 *   <li>{@link Coral.CoralMessages.CoralIntervalMessages.UnwantedIncident#getTransmissionFrequency <em>Transmission Frequency</em>}</li>
 *   <li>{@link Coral.CoralMessages.CoralIntervalMessages.UnwantedIncident#getConsequence <em>Consequence</em>}</li>
 * </ul>
 *
 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage#getUnwantedIncident()
 * @model
 * @generated
 */
public interface UnwantedIncident extends EObject {
	/**
	 * Returns the value of the '<em><b>Base Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Message</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Message</em>' reference.
	 * @see #setBase_Message(Message)
	 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage#getUnwantedIncident_Base_Message()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Message getBase_Message();

	/**
	 * Sets the value of the '{@link Coral.CoralMessages.CoralIntervalMessages.UnwantedIncident#getBase_Message <em>Base Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Message</em>' reference.
	 * @see #getBase_Message()
	 * @generated
	 */
	void setBase_Message(Message value);

	/**
	 * Returns the value of the '<em><b>Transmission Frequency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transmission Frequency</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transmission Frequency</em>' containment reference.
	 * @see #setTransmissionFrequency(IntFrequency)
	 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage#getUnwantedIncident_TransmissionFrequency()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	IntFrequency getTransmissionFrequency();

	/**
	 * Sets the value of the '{@link Coral.CoralMessages.CoralIntervalMessages.UnwantedIncident#getTransmissionFrequency <em>Transmission Frequency</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transmission Frequency</em>' containment reference.
	 * @see #getTransmissionFrequency()
	 * @generated
	 */
	void setTransmissionFrequency(IntFrequency value);

	/**
	 * Returns the value of the '<em><b>Consequence</b></em>' attribute.
	 * The literals are from the enumeration {@link Coral.CoralDataTypes.Consequences}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Consequence</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consequence</em>' attribute.
	 * @see Coral.CoralDataTypes.Consequences
	 * @see #setConsequence(Consequences)
	 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage#getUnwantedIncident_Consequence()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Consequences getConsequence();

	/**
	 * Sets the value of the '{@link Coral.CoralMessages.CoralIntervalMessages.UnwantedIncident#getConsequence <em>Consequence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Consequence</em>' attribute.
	 * @see Coral.CoralDataTypes.Consequences
	 * @see #getConsequence()
	 * @generated
	 */
	void setConsequence(Consequences value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * self.base_Message.receiveEvent.oclAsType(UML::MessageOccurrenceSpecification).covered->collect(l | l)->collect(l | l.extension_Asset).oclIsUndefined() = Bag{false}
	 * @param diagnostics The chain of diagnostics to which problems are to be appended.
	 * @param context The cache of context-specific information.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean RecepientIsAsset(DiagnosticChain diagnostics, Map context);

} // UnwantedIncident
