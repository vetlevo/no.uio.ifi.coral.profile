/**
 */
package Coral.CoralMessages.CoralIntervalMessages;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>General Message</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage#getGeneralMessage()
 * @model
 * @generated
 */
public interface GeneralMessage extends RiskyMessage {
} // GeneralMessage
