/**
 */
package Coral.CoralMessages;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see Coral.CoralMessages.CoralMessagesPackage
 * @generated
 */
public interface CoralMessagesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoralMessagesFactory eINSTANCE = Coral.CoralMessages.impl.CoralMessagesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Deleted Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deleted Message</em>'.
	 * @generated
	 */
	DeletedMessage createDeletedMessage();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CoralMessagesPackage getCoralMessagesPackage();

} //CoralMessagesFactory
