/**
 */
package Coral.CoralMessages;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Coral.CoralMessages.CoralMessagesFactory
 * @model kind="package"
 * @generated
 */
public interface CoralMessagesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "CoralMessages";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///Coral/CoralMessages.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Coral.CoralMessages";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoralMessagesPackage eINSTANCE = Coral.CoralMessages.impl.CoralMessagesPackageImpl.init();

	/**
	 * The meta object id for the '{@link Coral.CoralMessages.impl.DeletedMessageImpl <em>Deleted Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralMessages.impl.DeletedMessageImpl
	 * @see Coral.CoralMessages.impl.CoralMessagesPackageImpl#getDeletedMessage()
	 * @generated
	 */
	int DELETED_MESSAGE = 0;

	/**
	 * The feature id for the '<em><b>Base Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETED_MESSAGE__BASE_MESSAGE = 0;

	/**
	 * The number of structural features of the '<em>Deleted Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETED_MESSAGE_FEATURE_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link Coral.CoralMessages.DeletedMessage <em>Deleted Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deleted Message</em>'.
	 * @see Coral.CoralMessages.DeletedMessage
	 * @generated
	 */
	EClass getDeletedMessage();

	/**
	 * Returns the meta object for the reference '{@link Coral.CoralMessages.DeletedMessage#getBase_Message <em>Base Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Message</em>'.
	 * @see Coral.CoralMessages.DeletedMessage#getBase_Message()
	 * @see #getDeletedMessage()
	 * @generated
	 */
	EReference getDeletedMessage_Base_Message();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CoralMessagesFactory getCoralMessagesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Coral.CoralMessages.impl.DeletedMessageImpl <em>Deleted Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralMessages.impl.DeletedMessageImpl
		 * @see Coral.CoralMessages.impl.CoralMessagesPackageImpl#getDeletedMessage()
		 * @generated
		 */
		EClass DELETED_MESSAGE = eINSTANCE.getDeletedMessage();

		/**
		 * The meta object literal for the '<em><b>Base Message</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DELETED_MESSAGE__BASE_MESSAGE = eINSTANCE.getDeletedMessage_Base_Message();

	}

} //CoralMessagesPackage
