/**
 */
package Coral.CoralMessages.CoralExactMessages;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>New Message</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Coral.CoralMessages.CoralExactMessages.CoralExactMessagesPackage#getNewMessage()
 * @model
 * @generated
 */
public interface NewMessage extends RiskyMessage {
} // NewMessage
