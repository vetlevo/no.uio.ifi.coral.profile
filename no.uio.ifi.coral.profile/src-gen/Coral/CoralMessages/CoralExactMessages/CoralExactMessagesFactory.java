/**
 */
package Coral.CoralMessages.CoralExactMessages;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see Coral.CoralMessages.CoralExactMessages.CoralExactMessagesPackage
 * @generated
 */
public interface CoralExactMessagesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoralExactMessagesFactory eINSTANCE = Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>General Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>General Message</em>'.
	 * @generated
	 */
	GeneralMessage createGeneralMessage();

	/**
	 * Returns a new object of class '<em>New Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>New Message</em>'.
	 * @generated
	 */
	NewMessage createNewMessage();

	/**
	 * Returns a new object of class '<em>Altered Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Altered Message</em>'.
	 * @generated
	 */
	AlteredMessage createAlteredMessage();

	/**
	 * Returns a new object of class '<em>Unwanted Incident</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unwanted Incident</em>'.
	 * @generated
	 */
	UnwantedIncident createUnwantedIncident();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CoralExactMessagesPackage getCoralExactMessagesPackage();

} //CoralExactMessagesFactory
