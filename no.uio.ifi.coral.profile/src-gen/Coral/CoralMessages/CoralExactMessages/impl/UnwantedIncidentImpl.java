/**
 */
package Coral.CoralMessages.CoralExactMessages.impl;

import Coral.CoralDataTypes.Consequences;
import Coral.CoralDataTypes.ExactFrequency;

import Coral.CoralMessages.CoralExactMessages.CoralExactMessagesPackage;
import Coral.CoralMessages.CoralExactMessages.UnwantedIncident;

import Coral.CoralMessages.CoralExactMessages.util.CoralExactMessagesValidator;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

import org.eclipse.uml2.uml.Message;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unwanted Incident</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Coral.CoralMessages.CoralExactMessages.impl.UnwantedIncidentImpl#getTransmissionFrequency <em>Transmission Frequency</em>}</li>
 *   <li>{@link Coral.CoralMessages.CoralExactMessages.impl.UnwantedIncidentImpl#getConsequence <em>Consequence</em>}</li>
 *   <li>{@link Coral.CoralMessages.CoralExactMessages.impl.UnwantedIncidentImpl#getBase_Message <em>Base Message</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UnwantedIncidentImpl extends MinimalEObjectImpl.Container implements UnwantedIncident {
	/**
	 * The cached value of the '{@link #getTransmissionFrequency() <em>Transmission Frequency</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransmissionFrequency()
	 * @generated
	 * @ordered
	 */
	protected ExactFrequency transmissionFrequency;

	/**
	 * The default value of the '{@link #getConsequence() <em>Consequence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConsequence()
	 * @generated
	 * @ordered
	 */
	protected static final Consequences CONSEQUENCE_EDEFAULT = Consequences.INSIGNIFICANT_LITERAL;

	/**
	 * The cached value of the '{@link #getConsequence() <em>Consequence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConsequence()
	 * @generated
	 * @ordered
	 */
	protected Consequences consequence = CONSEQUENCE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBase_Message() <em>Base Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Message()
	 * @generated
	 * @ordered
	 */
	protected Message base_Message;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnwantedIncidentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CoralExactMessagesPackage.Literals.UNWANTED_INCIDENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExactFrequency getTransmissionFrequency() {
		return transmissionFrequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTransmissionFrequency(ExactFrequency newTransmissionFrequency, NotificationChain msgs) {
		ExactFrequency oldTransmissionFrequency = transmissionFrequency;
		transmissionFrequency = newTransmissionFrequency;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoralExactMessagesPackage.UNWANTED_INCIDENT__TRANSMISSION_FREQUENCY, oldTransmissionFrequency, newTransmissionFrequency);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransmissionFrequency(ExactFrequency newTransmissionFrequency) {
		if (newTransmissionFrequency != transmissionFrequency) {
			NotificationChain msgs = null;
			if (transmissionFrequency != null)
				msgs = ((InternalEObject)transmissionFrequency).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoralExactMessagesPackage.UNWANTED_INCIDENT__TRANSMISSION_FREQUENCY, null, msgs);
			if (newTransmissionFrequency != null)
				msgs = ((InternalEObject)newTransmissionFrequency).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CoralExactMessagesPackage.UNWANTED_INCIDENT__TRANSMISSION_FREQUENCY, null, msgs);
			msgs = basicSetTransmissionFrequency(newTransmissionFrequency, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoralExactMessagesPackage.UNWANTED_INCIDENT__TRANSMISSION_FREQUENCY, newTransmissionFrequency, newTransmissionFrequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Consequences getConsequence() {
		return consequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConsequence(Consequences newConsequence) {
		Consequences oldConsequence = consequence;
		consequence = newConsequence == null ? CONSEQUENCE_EDEFAULT : newConsequence;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoralExactMessagesPackage.UNWANTED_INCIDENT__CONSEQUENCE, oldConsequence, consequence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getBase_Message() {
		if (base_Message != null && base_Message.eIsProxy()) {
			InternalEObject oldBase_Message = (InternalEObject)base_Message;
			base_Message = (Message)eResolveProxy(oldBase_Message);
			if (base_Message != oldBase_Message) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoralExactMessagesPackage.UNWANTED_INCIDENT__BASE_MESSAGE, oldBase_Message, base_Message));
			}
		}
		return base_Message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message basicGetBase_Message() {
		return base_Message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Message(Message newBase_Message) {
		Message oldBase_Message = base_Message;
		base_Message = newBase_Message;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoralExactMessagesPackage.UNWANTED_INCIDENT__BASE_MESSAGE, oldBase_Message, base_Message));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean RecepientIsAsset(DiagnosticChain diagnostics, Map context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 CoralExactMessagesValidator.DIAGNOSTIC_SOURCE,
						 CoralExactMessagesValidator.UNWANTED_INCIDENT__RECEPIENT_IS_ASSET,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "RecepientIsAsset", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoralExactMessagesPackage.UNWANTED_INCIDENT__TRANSMISSION_FREQUENCY:
				return basicSetTransmissionFrequency(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoralExactMessagesPackage.UNWANTED_INCIDENT__TRANSMISSION_FREQUENCY:
				return getTransmissionFrequency();
			case CoralExactMessagesPackage.UNWANTED_INCIDENT__CONSEQUENCE:
				return getConsequence();
			case CoralExactMessagesPackage.UNWANTED_INCIDENT__BASE_MESSAGE:
				if (resolve) return getBase_Message();
				return basicGetBase_Message();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoralExactMessagesPackage.UNWANTED_INCIDENT__TRANSMISSION_FREQUENCY:
				setTransmissionFrequency((ExactFrequency)newValue);
				return;
			case CoralExactMessagesPackage.UNWANTED_INCIDENT__CONSEQUENCE:
				setConsequence((Consequences)newValue);
				return;
			case CoralExactMessagesPackage.UNWANTED_INCIDENT__BASE_MESSAGE:
				setBase_Message((Message)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoralExactMessagesPackage.UNWANTED_INCIDENT__TRANSMISSION_FREQUENCY:
				setTransmissionFrequency((ExactFrequency)null);
				return;
			case CoralExactMessagesPackage.UNWANTED_INCIDENT__CONSEQUENCE:
				setConsequence(CONSEQUENCE_EDEFAULT);
				return;
			case CoralExactMessagesPackage.UNWANTED_INCIDENT__BASE_MESSAGE:
				setBase_Message((Message)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoralExactMessagesPackage.UNWANTED_INCIDENT__TRANSMISSION_FREQUENCY:
				return transmissionFrequency != null;
			case CoralExactMessagesPackage.UNWANTED_INCIDENT__CONSEQUENCE:
				return consequence != CONSEQUENCE_EDEFAULT;
			case CoralExactMessagesPackage.UNWANTED_INCIDENT__BASE_MESSAGE:
				return base_Message != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (consequence: ");
		result.append(consequence);
		result.append(')');
		return result.toString();
	}

} //UnwantedIncidentImpl
