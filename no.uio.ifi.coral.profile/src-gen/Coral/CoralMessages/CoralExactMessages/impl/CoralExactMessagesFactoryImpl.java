/**
 */
package Coral.CoralMessages.CoralExactMessages.impl;

import Coral.CoralMessages.CoralExactMessages.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoralExactMessagesFactoryImpl extends EFactoryImpl implements CoralExactMessagesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CoralExactMessagesFactory init() {
		try {
			CoralExactMessagesFactory theCoralExactMessagesFactory = (CoralExactMessagesFactory)EPackage.Registry.INSTANCE.getEFactory(CoralExactMessagesPackage.eNS_URI);
			if (theCoralExactMessagesFactory != null) {
				return theCoralExactMessagesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CoralExactMessagesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralExactMessagesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CoralExactMessagesPackage.GENERAL_MESSAGE: return createGeneralMessage();
			case CoralExactMessagesPackage.NEW_MESSAGE: return createNewMessage();
			case CoralExactMessagesPackage.ALTERED_MESSAGE: return createAlteredMessage();
			case CoralExactMessagesPackage.UNWANTED_INCIDENT: return createUnwantedIncident();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneralMessage createGeneralMessage() {
		GeneralMessageImpl generalMessage = new GeneralMessageImpl();
		return generalMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NewMessage createNewMessage() {
		NewMessageImpl newMessage = new NewMessageImpl();
		return newMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlteredMessage createAlteredMessage() {
		AlteredMessageImpl alteredMessage = new AlteredMessageImpl();
		return alteredMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnwantedIncident createUnwantedIncident() {
		UnwantedIncidentImpl unwantedIncident = new UnwantedIncidentImpl();
		return unwantedIncident;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralExactMessagesPackage getCoralExactMessagesPackage() {
		return (CoralExactMessagesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static CoralExactMessagesPackage getPackage() {
		return CoralExactMessagesPackage.eINSTANCE;
	}

} //CoralExactMessagesFactoryImpl
