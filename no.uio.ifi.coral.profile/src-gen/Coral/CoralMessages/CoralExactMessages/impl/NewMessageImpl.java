/**
 */
package Coral.CoralMessages.CoralExactMessages.impl;

import Coral.CoralMessages.CoralExactMessages.CoralExactMessagesPackage;
import Coral.CoralMessages.CoralExactMessages.NewMessage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>New Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NewMessageImpl extends RiskyMessageImpl implements NewMessage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NewMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CoralExactMessagesPackage.Literals.NEW_MESSAGE;
	}

} //NewMessageImpl
