/**
 */
package Coral.CoralMessages.CoralExactMessages.impl;

import Coral.CoralMessages.CoralExactMessages.CoralExactMessagesPackage;
import Coral.CoralMessages.CoralExactMessages.GeneralMessage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>General Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GeneralMessageImpl extends RiskyMessageImpl implements GeneralMessage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GeneralMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CoralExactMessagesPackage.Literals.GENERAL_MESSAGE;
	}

} //GeneralMessageImpl
