/**
 */
package Coral.CoralMessages.CoralExactMessages;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>General Message</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Coral.CoralMessages.CoralExactMessages.CoralExactMessagesPackage#getGeneralMessage()
 * @model
 * @generated
 */
public interface GeneralMessage extends RiskyMessage {
} // GeneralMessage
