/**
 */
package Coral.CoralMessages.CoralExactMessages;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Coral.CoralMessages.CoralExactMessages.CoralExactMessagesFactory
 * @model kind="package"
 * @generated
 */
public interface CoralExactMessagesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "CoralExactMessages";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///Coral/CoralMessages/CoralExactMessages.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Coral.CoralMessages.CoralExactMessages";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoralExactMessagesPackage eINSTANCE = Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl.init();

	/**
	 * The meta object id for the '{@link Coral.CoralMessages.CoralExactMessages.impl.RiskyMessageImpl <em>Risky Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralMessages.CoralExactMessages.impl.RiskyMessageImpl
	 * @see Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl#getRiskyMessage()
	 * @generated
	 */
	int RISKY_MESSAGE = 0;

	/**
	 * The feature id for the '<em><b>Transmission Frequency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISKY_MESSAGE__TRANSMISSION_FREQUENCY = 0;

	/**
	 * The feature id for the '<em><b>Reception Frequency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISKY_MESSAGE__RECEPTION_FREQUENCY = 1;

	/**
	 * The feature id for the '<em><b>Conditional Ratio</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISKY_MESSAGE__CONDITIONAL_RATIO = 2;

	/**
	 * The feature id for the '<em><b>Base Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISKY_MESSAGE__BASE_MESSAGE = 3;

	/**
	 * The number of structural features of the '<em>Risky Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISKY_MESSAGE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link Coral.CoralMessages.CoralExactMessages.impl.GeneralMessageImpl <em>General Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralMessages.CoralExactMessages.impl.GeneralMessageImpl
	 * @see Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl#getGeneralMessage()
	 * @generated
	 */
	int GENERAL_MESSAGE = 1;

	/**
	 * The feature id for the '<em><b>Transmission Frequency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_MESSAGE__TRANSMISSION_FREQUENCY = RISKY_MESSAGE__TRANSMISSION_FREQUENCY;

	/**
	 * The feature id for the '<em><b>Reception Frequency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_MESSAGE__RECEPTION_FREQUENCY = RISKY_MESSAGE__RECEPTION_FREQUENCY;

	/**
	 * The feature id for the '<em><b>Conditional Ratio</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_MESSAGE__CONDITIONAL_RATIO = RISKY_MESSAGE__CONDITIONAL_RATIO;

	/**
	 * The feature id for the '<em><b>Base Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_MESSAGE__BASE_MESSAGE = RISKY_MESSAGE__BASE_MESSAGE;

	/**
	 * The number of structural features of the '<em>General Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_MESSAGE_FEATURE_COUNT = RISKY_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link Coral.CoralMessages.CoralExactMessages.impl.NewMessageImpl <em>New Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralMessages.CoralExactMessages.impl.NewMessageImpl
	 * @see Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl#getNewMessage()
	 * @generated
	 */
	int NEW_MESSAGE = 2;

	/**
	 * The feature id for the '<em><b>Transmission Frequency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_MESSAGE__TRANSMISSION_FREQUENCY = RISKY_MESSAGE__TRANSMISSION_FREQUENCY;

	/**
	 * The feature id for the '<em><b>Reception Frequency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_MESSAGE__RECEPTION_FREQUENCY = RISKY_MESSAGE__RECEPTION_FREQUENCY;

	/**
	 * The feature id for the '<em><b>Conditional Ratio</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_MESSAGE__CONDITIONAL_RATIO = RISKY_MESSAGE__CONDITIONAL_RATIO;

	/**
	 * The feature id for the '<em><b>Base Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_MESSAGE__BASE_MESSAGE = RISKY_MESSAGE__BASE_MESSAGE;

	/**
	 * The number of structural features of the '<em>New Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_MESSAGE_FEATURE_COUNT = RISKY_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link Coral.CoralMessages.CoralExactMessages.impl.AlteredMessageImpl <em>Altered Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralMessages.CoralExactMessages.impl.AlteredMessageImpl
	 * @see Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl#getAlteredMessage()
	 * @generated
	 */
	int ALTERED_MESSAGE = 3;

	/**
	 * The feature id for the '<em><b>Transmission Frequency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERED_MESSAGE__TRANSMISSION_FREQUENCY = RISKY_MESSAGE__TRANSMISSION_FREQUENCY;

	/**
	 * The feature id for the '<em><b>Reception Frequency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERED_MESSAGE__RECEPTION_FREQUENCY = RISKY_MESSAGE__RECEPTION_FREQUENCY;

	/**
	 * The feature id for the '<em><b>Conditional Ratio</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERED_MESSAGE__CONDITIONAL_RATIO = RISKY_MESSAGE__CONDITIONAL_RATIO;

	/**
	 * The feature id for the '<em><b>Base Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERED_MESSAGE__BASE_MESSAGE = RISKY_MESSAGE__BASE_MESSAGE;

	/**
	 * The number of structural features of the '<em>Altered Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALTERED_MESSAGE_FEATURE_COUNT = RISKY_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link Coral.CoralMessages.CoralExactMessages.impl.UnwantedIncidentImpl <em>Unwanted Incident</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralMessages.CoralExactMessages.impl.UnwantedIncidentImpl
	 * @see Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl#getUnwantedIncident()
	 * @generated
	 */
	int UNWANTED_INCIDENT = 4;

	/**
	 * The feature id for the '<em><b>Transmission Frequency</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNWANTED_INCIDENT__TRANSMISSION_FREQUENCY = 0;

	/**
	 * The feature id for the '<em><b>Consequence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNWANTED_INCIDENT__CONSEQUENCE = 1;

	/**
	 * The feature id for the '<em><b>Base Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNWANTED_INCIDENT__BASE_MESSAGE = 2;

	/**
	 * The number of structural features of the '<em>Unwanted Incident</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNWANTED_INCIDENT_FEATURE_COUNT = 3;


	/**
	 * Returns the meta object for class '{@link Coral.CoralMessages.CoralExactMessages.RiskyMessage <em>Risky Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Risky Message</em>'.
	 * @see Coral.CoralMessages.CoralExactMessages.RiskyMessage
	 * @generated
	 */
	EClass getRiskyMessage();

	/**
	 * Returns the meta object for the containment reference '{@link Coral.CoralMessages.CoralExactMessages.RiskyMessage#getTransmissionFrequency <em>Transmission Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Transmission Frequency</em>'.
	 * @see Coral.CoralMessages.CoralExactMessages.RiskyMessage#getTransmissionFrequency()
	 * @see #getRiskyMessage()
	 * @generated
	 */
	EReference getRiskyMessage_TransmissionFrequency();

	/**
	 * Returns the meta object for the containment reference '{@link Coral.CoralMessages.CoralExactMessages.RiskyMessage#getReceptionFrequency <em>Reception Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reception Frequency</em>'.
	 * @see Coral.CoralMessages.CoralExactMessages.RiskyMessage#getReceptionFrequency()
	 * @see #getRiskyMessage()
	 * @generated
	 */
	EReference getRiskyMessage_ReceptionFrequency();

	/**
	 * Returns the meta object for the containment reference '{@link Coral.CoralMessages.CoralExactMessages.RiskyMessage#getConditionalRatio <em>Conditional Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Conditional Ratio</em>'.
	 * @see Coral.CoralMessages.CoralExactMessages.RiskyMessage#getConditionalRatio()
	 * @see #getRiskyMessage()
	 * @generated
	 */
	EReference getRiskyMessage_ConditionalRatio();

	/**
	 * Returns the meta object for the reference '{@link Coral.CoralMessages.CoralExactMessages.RiskyMessage#getBase_Message <em>Base Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Message</em>'.
	 * @see Coral.CoralMessages.CoralExactMessages.RiskyMessage#getBase_Message()
	 * @see #getRiskyMessage()
	 * @generated
	 */
	EReference getRiskyMessage_Base_Message();

	/**
	 * Returns the meta object for class '{@link Coral.CoralMessages.CoralExactMessages.GeneralMessage <em>General Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>General Message</em>'.
	 * @see Coral.CoralMessages.CoralExactMessages.GeneralMessage
	 * @generated
	 */
	EClass getGeneralMessage();

	/**
	 * Returns the meta object for class '{@link Coral.CoralMessages.CoralExactMessages.NewMessage <em>New Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>New Message</em>'.
	 * @see Coral.CoralMessages.CoralExactMessages.NewMessage
	 * @generated
	 */
	EClass getNewMessage();

	/**
	 * Returns the meta object for class '{@link Coral.CoralMessages.CoralExactMessages.AlteredMessage <em>Altered Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Altered Message</em>'.
	 * @see Coral.CoralMessages.CoralExactMessages.AlteredMessage
	 * @generated
	 */
	EClass getAlteredMessage();

	/**
	 * Returns the meta object for class '{@link Coral.CoralMessages.CoralExactMessages.UnwantedIncident <em>Unwanted Incident</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unwanted Incident</em>'.
	 * @see Coral.CoralMessages.CoralExactMessages.UnwantedIncident
	 * @generated
	 */
	EClass getUnwantedIncident();

	/**
	 * Returns the meta object for the containment reference '{@link Coral.CoralMessages.CoralExactMessages.UnwantedIncident#getTransmissionFrequency <em>Transmission Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Transmission Frequency</em>'.
	 * @see Coral.CoralMessages.CoralExactMessages.UnwantedIncident#getTransmissionFrequency()
	 * @see #getUnwantedIncident()
	 * @generated
	 */
	EReference getUnwantedIncident_TransmissionFrequency();

	/**
	 * Returns the meta object for the attribute '{@link Coral.CoralMessages.CoralExactMessages.UnwantedIncident#getConsequence <em>Consequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Consequence</em>'.
	 * @see Coral.CoralMessages.CoralExactMessages.UnwantedIncident#getConsequence()
	 * @see #getUnwantedIncident()
	 * @generated
	 */
	EAttribute getUnwantedIncident_Consequence();

	/**
	 * Returns the meta object for the reference '{@link Coral.CoralMessages.CoralExactMessages.UnwantedIncident#getBase_Message <em>Base Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Message</em>'.
	 * @see Coral.CoralMessages.CoralExactMessages.UnwantedIncident#getBase_Message()
	 * @see #getUnwantedIncident()
	 * @generated
	 */
	EReference getUnwantedIncident_Base_Message();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CoralExactMessagesFactory getCoralExactMessagesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Coral.CoralMessages.CoralExactMessages.impl.RiskyMessageImpl <em>Risky Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralMessages.CoralExactMessages.impl.RiskyMessageImpl
		 * @see Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl#getRiskyMessage()
		 * @generated
		 */
		EClass RISKY_MESSAGE = eINSTANCE.getRiskyMessage();

		/**
		 * The meta object literal for the '<em><b>Transmission Frequency</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RISKY_MESSAGE__TRANSMISSION_FREQUENCY = eINSTANCE.getRiskyMessage_TransmissionFrequency();

		/**
		 * The meta object literal for the '<em><b>Reception Frequency</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RISKY_MESSAGE__RECEPTION_FREQUENCY = eINSTANCE.getRiskyMessage_ReceptionFrequency();

		/**
		 * The meta object literal for the '<em><b>Conditional Ratio</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RISKY_MESSAGE__CONDITIONAL_RATIO = eINSTANCE.getRiskyMessage_ConditionalRatio();

		/**
		 * The meta object literal for the '<em><b>Base Message</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RISKY_MESSAGE__BASE_MESSAGE = eINSTANCE.getRiskyMessage_Base_Message();

		/**
		 * The meta object literal for the '{@link Coral.CoralMessages.CoralExactMessages.impl.GeneralMessageImpl <em>General Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralMessages.CoralExactMessages.impl.GeneralMessageImpl
		 * @see Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl#getGeneralMessage()
		 * @generated
		 */
		EClass GENERAL_MESSAGE = eINSTANCE.getGeneralMessage();

		/**
		 * The meta object literal for the '{@link Coral.CoralMessages.CoralExactMessages.impl.NewMessageImpl <em>New Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralMessages.CoralExactMessages.impl.NewMessageImpl
		 * @see Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl#getNewMessage()
		 * @generated
		 */
		EClass NEW_MESSAGE = eINSTANCE.getNewMessage();

		/**
		 * The meta object literal for the '{@link Coral.CoralMessages.CoralExactMessages.impl.AlteredMessageImpl <em>Altered Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralMessages.CoralExactMessages.impl.AlteredMessageImpl
		 * @see Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl#getAlteredMessage()
		 * @generated
		 */
		EClass ALTERED_MESSAGE = eINSTANCE.getAlteredMessage();

		/**
		 * The meta object literal for the '{@link Coral.CoralMessages.CoralExactMessages.impl.UnwantedIncidentImpl <em>Unwanted Incident</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralMessages.CoralExactMessages.impl.UnwantedIncidentImpl
		 * @see Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl#getUnwantedIncident()
		 * @generated
		 */
		EClass UNWANTED_INCIDENT = eINSTANCE.getUnwantedIncident();

		/**
		 * The meta object literal for the '<em><b>Transmission Frequency</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNWANTED_INCIDENT__TRANSMISSION_FREQUENCY = eINSTANCE.getUnwantedIncident_TransmissionFrequency();

		/**
		 * The meta object literal for the '<em><b>Consequence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNWANTED_INCIDENT__CONSEQUENCE = eINSTANCE.getUnwantedIncident_Consequence();

		/**
		 * The meta object literal for the '<em><b>Base Message</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNWANTED_INCIDENT__BASE_MESSAGE = eINSTANCE.getUnwantedIncident_Base_Message();

	}

} //CoralExactMessagesPackage
