/**
 */
package Coral.CoralDataTypes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Time Units</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see Coral.CoralDataTypes.CoralDataTypesPackage#getTimeUnits()
 * @model
 * @generated
 */
public final class TimeUnits extends AbstractEnumerator {
	/**
	 * The '<em><b>Year</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Year</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #YEAR_LITERAL
	 * @model name="Year"
	 * @generated
	 * @ordered
	 */
	public static final int YEAR = 0;

	/**
	 * The '<em><b>Month</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Month</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MONTH_LITERAL
	 * @model name="Month"
	 * @generated
	 * @ordered
	 */
	public static final int MONTH = 1;

	/**
	 * The '<em><b>Week</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Week</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WEEK_LITERAL
	 * @model name="Week"
	 * @generated
	 * @ordered
	 */
	public static final int WEEK = 2;

	/**
	 * The '<em><b>Day</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Day</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DAY_LITERAL
	 * @model name="Day"
	 * @generated
	 * @ordered
	 */
	public static final int DAY = 3;

	/**
	 * The '<em><b>Hour</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Hour</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HOUR_LITERAL
	 * @model name="Hour"
	 * @generated
	 * @ordered
	 */
	public static final int HOUR = 4;

	/**
	 * The '<em><b>Minute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Minute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MINUTE_LITERAL
	 * @model name="Minute"
	 * @generated
	 * @ordered
	 */
	public static final int MINUTE = 5;

	/**
	 * The '<em><b>Second</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Second</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SECOND_LITERAL
	 * @model name="Second"
	 * @generated
	 * @ordered
	 */
	public static final int SECOND = 6;

	/**
	 * The '<em><b>Year</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #YEAR
	 * @generated
	 * @ordered
	 */
	public static final TimeUnits YEAR_LITERAL = new TimeUnits(YEAR, "Year", "Year");

	/**
	 * The '<em><b>Month</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MONTH
	 * @generated
	 * @ordered
	 */
	public static final TimeUnits MONTH_LITERAL = new TimeUnits(MONTH, "Month", "Month");

	/**
	 * The '<em><b>Week</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WEEK
	 * @generated
	 * @ordered
	 */
	public static final TimeUnits WEEK_LITERAL = new TimeUnits(WEEK, "Week", "Week");

	/**
	 * The '<em><b>Day</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DAY
	 * @generated
	 * @ordered
	 */
	public static final TimeUnits DAY_LITERAL = new TimeUnits(DAY, "Day", "Day");

	/**
	 * The '<em><b>Hour</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HOUR
	 * @generated
	 * @ordered
	 */
	public static final TimeUnits HOUR_LITERAL = new TimeUnits(HOUR, "Hour", "Hour");

	/**
	 * The '<em><b>Minute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINUTE
	 * @generated
	 * @ordered
	 */
	public static final TimeUnits MINUTE_LITERAL = new TimeUnits(MINUTE, "Minute", "Minute");

	/**
	 * The '<em><b>Second</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SECOND
	 * @generated
	 * @ordered
	 */
	public static final TimeUnits SECOND_LITERAL = new TimeUnits(SECOND, "Second", "Second");

	/**
	 * An array of all the '<em><b>Time Units</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final TimeUnits[] VALUES_ARRAY =
		new TimeUnits[] {
			YEAR_LITERAL,
			MONTH_LITERAL,
			WEEK_LITERAL,
			DAY_LITERAL,
			HOUR_LITERAL,
			MINUTE_LITERAL,
			SECOND_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Time Units</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Time Units</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static TimeUnits get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			TimeUnits result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Time Units</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static TimeUnits getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			TimeUnits result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Time Units</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static TimeUnits get(int value) {
		switch (value) {
			case YEAR: return YEAR_LITERAL;
			case MONTH: return MONTH_LITERAL;
			case WEEK: return WEEK_LITERAL;
			case DAY: return DAY_LITERAL;
			case HOUR: return HOUR_LITERAL;
			case MINUTE: return MINUTE_LITERAL;
			case SECOND: return SECOND_LITERAL;
		}
		return null;
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private TimeUnits(int value, String name, String literal) {
		super(value, name, literal);
	}

} //TimeUnits
