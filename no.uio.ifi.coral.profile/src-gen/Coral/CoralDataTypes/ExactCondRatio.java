/**
 */
package Coral.CoralDataTypes;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exact Cond Ratio</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Coral.CoralDataTypes.ExactCondRatio#getCondRatio <em>Cond Ratio</em>}</li>
 * </ul>
 *
 * @see Coral.CoralDataTypes.CoralDataTypesPackage#getExactCondRatio()
 * @model
 * @generated
 */
public interface ExactCondRatio extends EObject {
	/**
	 * Returns the value of the '<em><b>Cond Ratio</b></em>' attribute.
	 * The default value is <code>"0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cond Ratio</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cond Ratio</em>' attribute.
	 * @see #setCondRatio(double)
	 * @see Coral.CoralDataTypes.CoralDataTypesPackage#getExactCondRatio_CondRatio()
	 * @model default="0.0" dataType="org.eclipse.uml2.types.Real" required="true" ordered="false"
	 * @generated
	 */
	double getCondRatio();

	/**
	 * Sets the value of the '{@link Coral.CoralDataTypes.ExactCondRatio#getCondRatio <em>Cond Ratio</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cond Ratio</em>' attribute.
	 * @see #getCondRatio()
	 * @generated
	 */
	void setCondRatio(double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * self.condRatio >= 0.0
	 * @param diagnostics The chain of diagnostics to which problems are to be appended.
	 * @param context The cache of context-specific information.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean CondRatioIsPositiveValue(DiagnosticChain diagnostics, Map context);

} // ExactCondRatio
