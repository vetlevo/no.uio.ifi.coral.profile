/**
 */
package Coral.CoralDataTypes.util;

import Coral.CoralDataTypes.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see Coral.CoralDataTypes.CoralDataTypesPackage
 * @generated
 */
public class CoralDataTypesAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CoralDataTypesPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralDataTypesAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = CoralDataTypesPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CoralDataTypesSwitch modelSwitch =
		new CoralDataTypesSwitch() {
			public Object caseIntFrequency(IntFrequency object) {
				return createIntFrequencyAdapter();
			}
			public Object caseFrequency(Frequency object) {
				return createFrequencyAdapter();
			}
			public Object caseExactFrequency(ExactFrequency object) {
				return createExactFrequencyAdapter();
			}
			public Object caseExactCondRatio(ExactCondRatio object) {
				return createExactCondRatioAdapter();
			}
			public Object caseIntCondRatio(IntCondRatio object) {
				return createIntCondRatioAdapter();
			}
			public Object defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	public Adapter createAdapter(Notifier target) {
		return (Adapter)modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link Coral.CoralDataTypes.IntFrequency <em>Int Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Coral.CoralDataTypes.IntFrequency
	 * @generated
	 */
	public Adapter createIntFrequencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Coral.CoralDataTypes.Frequency <em>Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Coral.CoralDataTypes.Frequency
	 * @generated
	 */
	public Adapter createFrequencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Coral.CoralDataTypes.ExactFrequency <em>Exact Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Coral.CoralDataTypes.ExactFrequency
	 * @generated
	 */
	public Adapter createExactFrequencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Coral.CoralDataTypes.ExactCondRatio <em>Exact Cond Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Coral.CoralDataTypes.ExactCondRatio
	 * @generated
	 */
	public Adapter createExactCondRatioAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Coral.CoralDataTypes.IntCondRatio <em>Int Cond Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Coral.CoralDataTypes.IntCondRatio
	 * @generated
	 */
	public Adapter createIntCondRatioAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //CoralDataTypesAdapterFactory
