/**
 */
package Coral.CoralDataTypes.util;

import Coral.CoralDataTypes.*;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see Coral.CoralDataTypes.CoralDataTypesPackage
 * @generated
 */
public class CoralDataTypesValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final CoralDataTypesValidator INSTANCE = new CoralDataTypesValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "Coral.CoralDataTypes";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Frequency Is Positive Value' of 'Int Frequency'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int INT_FREQUENCY__FREQUENCY_IS_POSITIVE_VALUE = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Frequency Is Positive Value' of 'Exact Frequency'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int EXACT_FREQUENCY__FREQUENCY_IS_POSITIVE_VALUE = 2;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Cond Ratio Is Positive Value' of 'Exact Cond Ratio'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int EXACT_COND_RATIO__COND_RATIO_IS_POSITIVE_VALUE = 3;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Cond Ratio Is Positive Value' of 'Int Cond Ratio'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int INT_COND_RATIO__COND_RATIO_IS_POSITIVE_VALUE = 4;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 4;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralDataTypesValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EPackage getEPackage() {
	  return CoralDataTypesPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map context) {
		switch (classifierID) {
			case CoralDataTypesPackage.INT_FREQUENCY:
				return validateIntFrequency((IntFrequency)value, diagnostics, context);
			case CoralDataTypesPackage.FREQUENCY:
				return validateFrequency((Frequency)value, diagnostics, context);
			case CoralDataTypesPackage.EXACT_FREQUENCY:
				return validateExactFrequency((ExactFrequency)value, diagnostics, context);
			case CoralDataTypesPackage.EXACT_COND_RATIO:
				return validateExactCondRatio((ExactCondRatio)value, diagnostics, context);
			case CoralDataTypesPackage.INT_COND_RATIO:
				return validateIntCondRatio((IntCondRatio)value, diagnostics, context);
			case CoralDataTypesPackage.TIME_UNITS:
				return validateTimeUnits((TimeUnits)value, diagnostics, context);
			case CoralDataTypesPackage.LIKELIHOODS:
				return validateLikelihoods((Likelihoods)value, diagnostics, context);
			case CoralDataTypesPackage.CONSEQUENCES:
				return validateConsequences((Consequences)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIntFrequency(IntFrequency intFrequency, DiagnosticChain diagnostics, Map context) {
		boolean result = validate_EveryMultiplicityConforms(intFrequency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(intFrequency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(intFrequency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(intFrequency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(intFrequency, diagnostics, context);
		if (result || diagnostics != null) result &= validateIntFrequency_FrequencyIsPositiveValue(intFrequency, diagnostics, context);
		return result;
	}

	/**
	 * Validates the FrequencyIsPositiveValue constraint of '<em>Int Frequency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIntFrequency_FrequencyIsPositiveValue(IntFrequency intFrequency, DiagnosticChain diagnostics, Map context) {
		return intFrequency.FrequencyIsPositiveValue(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFrequency(Frequency frequency, DiagnosticChain diagnostics, Map context) {
		return validate_EveryDefaultConstraint(frequency, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExactFrequency(ExactFrequency exactFrequency, DiagnosticChain diagnostics, Map context) {
		boolean result = validate_EveryMultiplicityConforms(exactFrequency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(exactFrequency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(exactFrequency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(exactFrequency, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(exactFrequency, diagnostics, context);
		if (result || diagnostics != null) result &= validateExactFrequency_FrequencyIsPositiveValue(exactFrequency, diagnostics, context);
		return result;
	}

	/**
	 * Validates the FrequencyIsPositiveValue constraint of '<em>Exact Frequency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExactFrequency_FrequencyIsPositiveValue(ExactFrequency exactFrequency, DiagnosticChain diagnostics, Map context) {
		return exactFrequency.FrequencyIsPositiveValue(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExactCondRatio(ExactCondRatio exactCondRatio, DiagnosticChain diagnostics, Map context) {
		boolean result = validate_EveryMultiplicityConforms(exactCondRatio, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(exactCondRatio, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(exactCondRatio, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(exactCondRatio, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(exactCondRatio, diagnostics, context);
		if (result || diagnostics != null) result &= validateExactCondRatio_CondRatioIsPositiveValue(exactCondRatio, diagnostics, context);
		return result;
	}

	/**
	 * Validates the CondRatioIsPositiveValue constraint of '<em>Exact Cond Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExactCondRatio_CondRatioIsPositiveValue(ExactCondRatio exactCondRatio, DiagnosticChain diagnostics, Map context) {
		return exactCondRatio.CondRatioIsPositiveValue(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIntCondRatio(IntCondRatio intCondRatio, DiagnosticChain diagnostics, Map context) {
		boolean result = validate_EveryMultiplicityConforms(intCondRatio, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(intCondRatio, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(intCondRatio, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(intCondRatio, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(intCondRatio, diagnostics, context);
		if (result || diagnostics != null) result &= validateIntCondRatio_CondRatioIsPositiveValue(intCondRatio, diagnostics, context);
		return result;
	}

	/**
	 * Validates the CondRatioIsPositiveValue constraint of '<em>Int Cond Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIntCondRatio_CondRatioIsPositiveValue(IntCondRatio intCondRatio, DiagnosticChain diagnostics, Map context) {
		return intCondRatio.CondRatioIsPositiveValue(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTimeUnits(TimeUnits timeUnits, DiagnosticChain diagnostics, Map context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLikelihoods(Likelihoods likelihoods, DiagnosticChain diagnostics, Map context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConsequences(Consequences consequences, DiagnosticChain diagnostics, Map context) {
		return true;
	}

} //CoralDataTypesValidator
