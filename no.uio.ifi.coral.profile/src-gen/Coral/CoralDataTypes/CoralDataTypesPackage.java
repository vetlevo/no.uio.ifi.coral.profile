/**
 */
package Coral.CoralDataTypes;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Coral.CoralDataTypes.CoralDataTypesFactory
 * @model kind="package"
 * @generated
 */
public interface CoralDataTypesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "CoralDataTypes";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///Coral/CoralDataTypes.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Coral.CoralDataTypes";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoralDataTypesPackage eINSTANCE = Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl.init();

	/**
	 * The meta object id for the '{@link Coral.CoralDataTypes.impl.FrequencyImpl <em>Frequency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralDataTypes.impl.FrequencyImpl
	 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getFrequency()
	 * @generated
	 */
	int FREQUENCY = 1;

	/**
	 * The feature id for the '<em><b>Timeunit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENCY__TIMEUNIT = 0;

	/**
	 * The feature id for the '<em><b>Likelihood</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENCY__LIKELIHOOD = 1;

	/**
	 * The number of structural features of the '<em>Frequency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREQUENCY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link Coral.CoralDataTypes.impl.IntFrequencyImpl <em>Int Frequency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralDataTypes.impl.IntFrequencyImpl
	 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getIntFrequency()
	 * @generated
	 */
	int INT_FREQUENCY = 0;

	/**
	 * The feature id for the '<em><b>Timeunit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_FREQUENCY__TIMEUNIT = FREQUENCY__TIMEUNIT;

	/**
	 * The feature id for the '<em><b>Likelihood</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_FREQUENCY__LIKELIHOOD = FREQUENCY__LIKELIHOOD;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_FREQUENCY__MIN = FREQUENCY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_FREQUENCY__MAX = FREQUENCY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Int Frequency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_FREQUENCY_FEATURE_COUNT = FREQUENCY_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link Coral.CoralDataTypes.impl.ExactFrequencyImpl <em>Exact Frequency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralDataTypes.impl.ExactFrequencyImpl
	 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getExactFrequency()
	 * @generated
	 */
	int EXACT_FREQUENCY = 2;

	/**
	 * The feature id for the '<em><b>Timeunit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXACT_FREQUENCY__TIMEUNIT = FREQUENCY__TIMEUNIT;

	/**
	 * The feature id for the '<em><b>Likelihood</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXACT_FREQUENCY__LIKELIHOOD = FREQUENCY__LIKELIHOOD;

	/**
	 * The feature id for the '<em><b>Exact</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXACT_FREQUENCY__EXACT = FREQUENCY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Exact Frequency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXACT_FREQUENCY_FEATURE_COUNT = FREQUENCY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link Coral.CoralDataTypes.impl.ExactCondRatioImpl <em>Exact Cond Ratio</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralDataTypes.impl.ExactCondRatioImpl
	 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getExactCondRatio()
	 * @generated
	 */
	int EXACT_COND_RATIO = 3;

	/**
	 * The feature id for the '<em><b>Cond Ratio</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXACT_COND_RATIO__COND_RATIO = 0;

	/**
	 * The number of structural features of the '<em>Exact Cond Ratio</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXACT_COND_RATIO_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link Coral.CoralDataTypes.impl.IntCondRatioImpl <em>Int Cond Ratio</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralDataTypes.impl.IntCondRatioImpl
	 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getIntCondRatio()
	 * @generated
	 */
	int INT_COND_RATIO = 4;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_COND_RATIO__MIN = 0;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_COND_RATIO__MAX = 1;

	/**
	 * The number of structural features of the '<em>Int Cond Ratio</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_COND_RATIO_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link Coral.CoralDataTypes.TimeUnits <em>Time Units</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralDataTypes.TimeUnits
	 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getTimeUnits()
	 * @generated
	 */
	int TIME_UNITS = 5;

	/**
	 * The meta object id for the '{@link Coral.CoralDataTypes.Likelihoods <em>Likelihoods</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralDataTypes.Likelihoods
	 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getLikelihoods()
	 * @generated
	 */
	int LIKELIHOODS = 6;

	/**
	 * The meta object id for the '{@link Coral.CoralDataTypes.Consequences <em>Consequences</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Coral.CoralDataTypes.Consequences
	 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getConsequences()
	 * @generated
	 */
	int CONSEQUENCES = 7;


	/**
	 * Returns the meta object for class '{@link Coral.CoralDataTypes.IntFrequency <em>Int Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Frequency</em>'.
	 * @see Coral.CoralDataTypes.IntFrequency
	 * @generated
	 */
	EClass getIntFrequency();

	/**
	 * Returns the meta object for the attribute '{@link Coral.CoralDataTypes.IntFrequency#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see Coral.CoralDataTypes.IntFrequency#getMin()
	 * @see #getIntFrequency()
	 * @generated
	 */
	EAttribute getIntFrequency_Min();

	/**
	 * Returns the meta object for the attribute '{@link Coral.CoralDataTypes.IntFrequency#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see Coral.CoralDataTypes.IntFrequency#getMax()
	 * @see #getIntFrequency()
	 * @generated
	 */
	EAttribute getIntFrequency_Max();

	/**
	 * Returns the meta object for class '{@link Coral.CoralDataTypes.Frequency <em>Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Frequency</em>'.
	 * @see Coral.CoralDataTypes.Frequency
	 * @generated
	 */
	EClass getFrequency();

	/**
	 * Returns the meta object for the attribute '{@link Coral.CoralDataTypes.Frequency#getTimeunit <em>Timeunit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timeunit</em>'.
	 * @see Coral.CoralDataTypes.Frequency#getTimeunit()
	 * @see #getFrequency()
	 * @generated
	 */
	EAttribute getFrequency_Timeunit();

	/**
	 * Returns the meta object for the attribute '{@link Coral.CoralDataTypes.Frequency#getLikelihood <em>Likelihood</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Likelihood</em>'.
	 * @see Coral.CoralDataTypes.Frequency#getLikelihood()
	 * @see #getFrequency()
	 * @generated
	 */
	EAttribute getFrequency_Likelihood();

	/**
	 * Returns the meta object for class '{@link Coral.CoralDataTypes.ExactFrequency <em>Exact Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exact Frequency</em>'.
	 * @see Coral.CoralDataTypes.ExactFrequency
	 * @generated
	 */
	EClass getExactFrequency();

	/**
	 * Returns the meta object for the attribute '{@link Coral.CoralDataTypes.ExactFrequency#getExact <em>Exact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exact</em>'.
	 * @see Coral.CoralDataTypes.ExactFrequency#getExact()
	 * @see #getExactFrequency()
	 * @generated
	 */
	EAttribute getExactFrequency_Exact();

	/**
	 * Returns the meta object for class '{@link Coral.CoralDataTypes.ExactCondRatio <em>Exact Cond Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exact Cond Ratio</em>'.
	 * @see Coral.CoralDataTypes.ExactCondRatio
	 * @generated
	 */
	EClass getExactCondRatio();

	/**
	 * Returns the meta object for the attribute '{@link Coral.CoralDataTypes.ExactCondRatio#getCondRatio <em>Cond Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cond Ratio</em>'.
	 * @see Coral.CoralDataTypes.ExactCondRatio#getCondRatio()
	 * @see #getExactCondRatio()
	 * @generated
	 */
	EAttribute getExactCondRatio_CondRatio();

	/**
	 * Returns the meta object for class '{@link Coral.CoralDataTypes.IntCondRatio <em>Int Cond Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Cond Ratio</em>'.
	 * @see Coral.CoralDataTypes.IntCondRatio
	 * @generated
	 */
	EClass getIntCondRatio();

	/**
	 * Returns the meta object for the attribute '{@link Coral.CoralDataTypes.IntCondRatio#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see Coral.CoralDataTypes.IntCondRatio#getMin()
	 * @see #getIntCondRatio()
	 * @generated
	 */
	EAttribute getIntCondRatio_Min();

	/**
	 * Returns the meta object for the attribute '{@link Coral.CoralDataTypes.IntCondRatio#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see Coral.CoralDataTypes.IntCondRatio#getMax()
	 * @see #getIntCondRatio()
	 * @generated
	 */
	EAttribute getIntCondRatio_Max();

	/**
	 * Returns the meta object for enum '{@link Coral.CoralDataTypes.TimeUnits <em>Time Units</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Time Units</em>'.
	 * @see Coral.CoralDataTypes.TimeUnits
	 * @generated
	 */
	EEnum getTimeUnits();

	/**
	 * Returns the meta object for enum '{@link Coral.CoralDataTypes.Likelihoods <em>Likelihoods</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Likelihoods</em>'.
	 * @see Coral.CoralDataTypes.Likelihoods
	 * @generated
	 */
	EEnum getLikelihoods();

	/**
	 * Returns the meta object for enum '{@link Coral.CoralDataTypes.Consequences <em>Consequences</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Consequences</em>'.
	 * @see Coral.CoralDataTypes.Consequences
	 * @generated
	 */
	EEnum getConsequences();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CoralDataTypesFactory getCoralDataTypesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Coral.CoralDataTypes.impl.IntFrequencyImpl <em>Int Frequency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralDataTypes.impl.IntFrequencyImpl
		 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getIntFrequency()
		 * @generated
		 */
		EClass INT_FREQUENCY = eINSTANCE.getIntFrequency();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_FREQUENCY__MIN = eINSTANCE.getIntFrequency_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_FREQUENCY__MAX = eINSTANCE.getIntFrequency_Max();

		/**
		 * The meta object literal for the '{@link Coral.CoralDataTypes.impl.FrequencyImpl <em>Frequency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralDataTypes.impl.FrequencyImpl
		 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getFrequency()
		 * @generated
		 */
		EClass FREQUENCY = eINSTANCE.getFrequency();

		/**
		 * The meta object literal for the '<em><b>Timeunit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREQUENCY__TIMEUNIT = eINSTANCE.getFrequency_Timeunit();

		/**
		 * The meta object literal for the '<em><b>Likelihood</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREQUENCY__LIKELIHOOD = eINSTANCE.getFrequency_Likelihood();

		/**
		 * The meta object literal for the '{@link Coral.CoralDataTypes.impl.ExactFrequencyImpl <em>Exact Frequency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralDataTypes.impl.ExactFrequencyImpl
		 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getExactFrequency()
		 * @generated
		 */
		EClass EXACT_FREQUENCY = eINSTANCE.getExactFrequency();

		/**
		 * The meta object literal for the '<em><b>Exact</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXACT_FREQUENCY__EXACT = eINSTANCE.getExactFrequency_Exact();

		/**
		 * The meta object literal for the '{@link Coral.CoralDataTypes.impl.ExactCondRatioImpl <em>Exact Cond Ratio</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralDataTypes.impl.ExactCondRatioImpl
		 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getExactCondRatio()
		 * @generated
		 */
		EClass EXACT_COND_RATIO = eINSTANCE.getExactCondRatio();

		/**
		 * The meta object literal for the '<em><b>Cond Ratio</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXACT_COND_RATIO__COND_RATIO = eINSTANCE.getExactCondRatio_CondRatio();

		/**
		 * The meta object literal for the '{@link Coral.CoralDataTypes.impl.IntCondRatioImpl <em>Int Cond Ratio</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralDataTypes.impl.IntCondRatioImpl
		 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getIntCondRatio()
		 * @generated
		 */
		EClass INT_COND_RATIO = eINSTANCE.getIntCondRatio();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_COND_RATIO__MIN = eINSTANCE.getIntCondRatio_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_COND_RATIO__MAX = eINSTANCE.getIntCondRatio_Max();

		/**
		 * The meta object literal for the '{@link Coral.CoralDataTypes.TimeUnits <em>Time Units</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralDataTypes.TimeUnits
		 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getTimeUnits()
		 * @generated
		 */
		EEnum TIME_UNITS = eINSTANCE.getTimeUnits();

		/**
		 * The meta object literal for the '{@link Coral.CoralDataTypes.Likelihoods <em>Likelihoods</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralDataTypes.Likelihoods
		 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getLikelihoods()
		 * @generated
		 */
		EEnum LIKELIHOODS = eINSTANCE.getLikelihoods();

		/**
		 * The meta object literal for the '{@link Coral.CoralDataTypes.Consequences <em>Consequences</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Coral.CoralDataTypes.Consequences
		 * @see Coral.CoralDataTypes.impl.CoralDataTypesPackageImpl#getConsequences()
		 * @generated
		 */
		EEnum CONSEQUENCES = eINSTANCE.getConsequences();

	}

} //CoralDataTypesPackage
