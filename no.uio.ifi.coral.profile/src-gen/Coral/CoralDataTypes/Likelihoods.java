/**
 */
package Coral.CoralDataTypes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Likelihoods</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see Coral.CoralDataTypes.CoralDataTypesPackage#getLikelihoods()
 * @model
 * @generated
 */
public final class Likelihoods extends AbstractEnumerator {
	/**
	 * The '<em><b>Rare</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Rare</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RARE_LITERAL
	 * @model name="Rare"
	 * @generated
	 * @ordered
	 */
	public static final int RARE = 0;

	/**
	 * The '<em><b>Unlikely</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Unlikely</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNLIKELY_LITERAL
	 * @model name="Unlikely"
	 * @generated
	 * @ordered
	 */
	public static final int UNLIKELY = 1;

	/**
	 * The '<em><b>Possible</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Possible</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #POSSIBLE_LITERAL
	 * @model name="Possible"
	 * @generated
	 * @ordered
	 */
	public static final int POSSIBLE = 2;

	/**
	 * The '<em><b>Likely</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Likely</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LIKELY_LITERAL
	 * @model name="Likely"
	 * @generated
	 * @ordered
	 */
	public static final int LIKELY = 3;

	/**
	 * The '<em><b>Certain</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Certain</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CERTAIN_LITERAL
	 * @model name="Certain"
	 * @generated
	 * @ordered
	 */
	public static final int CERTAIN = 4;

	/**
	 * The '<em><b>Rare</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RARE
	 * @generated
	 * @ordered
	 */
	public static final Likelihoods RARE_LITERAL = new Likelihoods(RARE, "Rare", "Rare");

	/**
	 * The '<em><b>Unlikely</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNLIKELY
	 * @generated
	 * @ordered
	 */
	public static final Likelihoods UNLIKELY_LITERAL = new Likelihoods(UNLIKELY, "Unlikely", "Unlikely");

	/**
	 * The '<em><b>Possible</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #POSSIBLE
	 * @generated
	 * @ordered
	 */
	public static final Likelihoods POSSIBLE_LITERAL = new Likelihoods(POSSIBLE, "Possible", "Possible");

	/**
	 * The '<em><b>Likely</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LIKELY
	 * @generated
	 * @ordered
	 */
	public static final Likelihoods LIKELY_LITERAL = new Likelihoods(LIKELY, "Likely", "Likely");

	/**
	 * The '<em><b>Certain</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CERTAIN
	 * @generated
	 * @ordered
	 */
	public static final Likelihoods CERTAIN_LITERAL = new Likelihoods(CERTAIN, "Certain", "Certain");

	/**
	 * An array of all the '<em><b>Likelihoods</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final Likelihoods[] VALUES_ARRAY =
		new Likelihoods[] {
			RARE_LITERAL,
			UNLIKELY_LITERAL,
			POSSIBLE_LITERAL,
			LIKELY_LITERAL,
			CERTAIN_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Likelihoods</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Likelihoods</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Likelihoods get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Likelihoods result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Likelihoods</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Likelihoods getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Likelihoods result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Likelihoods</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Likelihoods get(int value) {
		switch (value) {
			case RARE: return RARE_LITERAL;
			case UNLIKELY: return UNLIKELY_LITERAL;
			case POSSIBLE: return POSSIBLE_LITERAL;
			case LIKELY: return LIKELY_LITERAL;
			case CERTAIN: return CERTAIN_LITERAL;
		}
		return null;
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Likelihoods(int value, String name, String literal) {
		super(value, name, literal);
	}

} //Likelihoods
