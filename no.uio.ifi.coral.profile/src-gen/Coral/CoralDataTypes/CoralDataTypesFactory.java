/**
 */
package Coral.CoralDataTypes;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see Coral.CoralDataTypes.CoralDataTypesPackage
 * @generated
 */
public interface CoralDataTypesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoralDataTypesFactory eINSTANCE = Coral.CoralDataTypes.impl.CoralDataTypesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Int Frequency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Frequency</em>'.
	 * @generated
	 */
	IntFrequency createIntFrequency();

	/**
	 * Returns a new object of class '<em>Frequency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Frequency</em>'.
	 * @generated
	 */
	Frequency createFrequency();

	/**
	 * Returns a new object of class '<em>Exact Frequency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exact Frequency</em>'.
	 * @generated
	 */
	ExactFrequency createExactFrequency();

	/**
	 * Returns a new object of class '<em>Exact Cond Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exact Cond Ratio</em>'.
	 * @generated
	 */
	ExactCondRatio createExactCondRatio();

	/**
	 * Returns a new object of class '<em>Int Cond Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Cond Ratio</em>'.
	 * @generated
	 */
	IntCondRatio createIntCondRatio();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CoralDataTypesPackage getCoralDataTypesPackage();

} //CoralDataTypesFactory
