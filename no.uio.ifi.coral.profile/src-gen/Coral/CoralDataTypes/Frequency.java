/**
 */
package Coral.CoralDataTypes;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Frequency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Coral.CoralDataTypes.Frequency#getTimeunit <em>Timeunit</em>}</li>
 *   <li>{@link Coral.CoralDataTypes.Frequency#getLikelihood <em>Likelihood</em>}</li>
 * </ul>
 *
 * @see Coral.CoralDataTypes.CoralDataTypesPackage#getFrequency()
 * @model
 * @generated
 */
public interface Frequency extends EObject {
	/**
	 * Returns the value of the '<em><b>Timeunit</b></em>' attribute.
	 * The literals are from the enumeration {@link Coral.CoralDataTypes.TimeUnits}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timeunit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timeunit</em>' attribute.
	 * @see Coral.CoralDataTypes.TimeUnits
	 * @see #setTimeunit(TimeUnits)
	 * @see Coral.CoralDataTypes.CoralDataTypesPackage#getFrequency_Timeunit()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	TimeUnits getTimeunit();

	/**
	 * Sets the value of the '{@link Coral.CoralDataTypes.Frequency#getTimeunit <em>Timeunit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timeunit</em>' attribute.
	 * @see Coral.CoralDataTypes.TimeUnits
	 * @see #getTimeunit()
	 * @generated
	 */
	void setTimeunit(TimeUnits value);

	/**
	 * Returns the value of the '<em><b>Likelihood</b></em>' attribute.
	 * The literals are from the enumeration {@link Coral.CoralDataTypes.Likelihoods}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Likelihood</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Likelihood</em>' attribute.
	 * @see Coral.CoralDataTypes.Likelihoods
	 * @see #setLikelihood(Likelihoods)
	 * @see Coral.CoralDataTypes.CoralDataTypesPackage#getFrequency_Likelihood()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Likelihoods getLikelihood();

	/**
	 * Sets the value of the '{@link Coral.CoralDataTypes.Frequency#getLikelihood <em>Likelihood</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Likelihood</em>' attribute.
	 * @see Coral.CoralDataTypes.Likelihoods
	 * @see #getLikelihood()
	 * @generated
	 */
	void setLikelihood(Likelihoods value);

} // Frequency
