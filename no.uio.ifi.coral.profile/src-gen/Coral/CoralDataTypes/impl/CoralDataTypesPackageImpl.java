/**
 */
package Coral.CoralDataTypes.impl;

import Coral.CoralDataTypes.Consequences;
import Coral.CoralDataTypes.CoralDataTypesFactory;
import Coral.CoralDataTypes.CoralDataTypesPackage;
import Coral.CoralDataTypes.ExactCondRatio;
import Coral.CoralDataTypes.ExactFrequency;
import Coral.CoralDataTypes.Frequency;
import Coral.CoralDataTypes.IntCondRatio;
import Coral.CoralDataTypes.IntFrequency;
import Coral.CoralDataTypes.Likelihoods;
import Coral.CoralDataTypes.TimeUnits;

import Coral.CoralDataTypes.util.CoralDataTypesValidator;

import Coral.CoralLifelines.CoralLifelinesPackage;

import Coral.CoralLifelines.impl.CoralLifelinesPackageImpl;

import Coral.CoralMessages.CoralExactMessages.CoralExactMessagesPackage;

import Coral.CoralMessages.CoralExactMessages.impl.CoralExactMessagesPackageImpl;

import Coral.CoralMessages.CoralIntervalMessages.CoralIntervalMessagesPackage;

import Coral.CoralMessages.CoralIntervalMessages.impl.CoralIntervalMessagesPackageImpl;

import Coral.CoralMessages.CoralMessagesPackage;

import Coral.CoralMessages.impl.CoralMessagesPackageImpl;

import Coral.CoralRiskMeasureAnnotations.CoralRiskMeasureAnnotationsPackage;

import Coral.CoralRiskMeasureAnnotations.impl.CoralRiskMeasureAnnotationsPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoralDataTypesPackageImpl extends EPackageImpl implements CoralDataTypesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intFrequencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass frequencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exactFrequencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exactCondRatioEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intCondRatioEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum timeUnitsEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum likelihoodsEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum consequencesEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see Coral.CoralDataTypes.CoralDataTypesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CoralDataTypesPackageImpl() {
		super(eNS_URI, CoralDataTypesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CoralDataTypesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CoralDataTypesPackage init() {
		if (isInited) return (CoralDataTypesPackage)EPackage.Registry.INSTANCE.getEPackage(CoralDataTypesPackage.eNS_URI);

		// Obtain or create and register package
		CoralDataTypesPackageImpl theCoralDataTypesPackage = (CoralDataTypesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CoralDataTypesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CoralDataTypesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CoralLifelinesPackageImpl theCoralLifelinesPackage = (CoralLifelinesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralLifelinesPackage.eNS_URI) instanceof CoralLifelinesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralLifelinesPackage.eNS_URI) : CoralLifelinesPackage.eINSTANCE);
		CoralMessagesPackageImpl theCoralMessagesPackage = (CoralMessagesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralMessagesPackage.eNS_URI) instanceof CoralMessagesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralMessagesPackage.eNS_URI) : CoralMessagesPackage.eINSTANCE);
		CoralIntervalMessagesPackageImpl theCoralIntervalMessagesPackage = (CoralIntervalMessagesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralIntervalMessagesPackage.eNS_URI) instanceof CoralIntervalMessagesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralIntervalMessagesPackage.eNS_URI) : CoralIntervalMessagesPackage.eINSTANCE);
		CoralExactMessagesPackageImpl theCoralExactMessagesPackage = (CoralExactMessagesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralExactMessagesPackage.eNS_URI) instanceof CoralExactMessagesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralExactMessagesPackage.eNS_URI) : CoralExactMessagesPackage.eINSTANCE);
		CoralRiskMeasureAnnotationsPackageImpl theCoralRiskMeasureAnnotationsPackage = (CoralRiskMeasureAnnotationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CoralRiskMeasureAnnotationsPackage.eNS_URI) instanceof CoralRiskMeasureAnnotationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CoralRiskMeasureAnnotationsPackage.eNS_URI) : CoralRiskMeasureAnnotationsPackage.eINSTANCE);

		// Create package meta-data objects
		theCoralDataTypesPackage.createPackageContents();
		theCoralLifelinesPackage.createPackageContents();
		theCoralMessagesPackage.createPackageContents();
		theCoralIntervalMessagesPackage.createPackageContents();
		theCoralExactMessagesPackage.createPackageContents();
		theCoralRiskMeasureAnnotationsPackage.createPackageContents();

		// Initialize created meta-data
		theCoralDataTypesPackage.initializePackageContents();
		theCoralLifelinesPackage.initializePackageContents();
		theCoralMessagesPackage.initializePackageContents();
		theCoralIntervalMessagesPackage.initializePackageContents();
		theCoralExactMessagesPackage.initializePackageContents();
		theCoralRiskMeasureAnnotationsPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theCoralDataTypesPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return CoralDataTypesValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theCoralDataTypesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CoralDataTypesPackage.eNS_URI, theCoralDataTypesPackage);
		return theCoralDataTypesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntFrequency() {
		return intFrequencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntFrequency_Min() {
		return (EAttribute)intFrequencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntFrequency_Max() {
		return (EAttribute)intFrequencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFrequency() {
		return frequencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFrequency_Timeunit() {
		return (EAttribute)frequencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFrequency_Likelihood() {
		return (EAttribute)frequencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExactFrequency() {
		return exactFrequencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExactFrequency_Exact() {
		return (EAttribute)exactFrequencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExactCondRatio() {
		return exactCondRatioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExactCondRatio_CondRatio() {
		return (EAttribute)exactCondRatioEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntCondRatio() {
		return intCondRatioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntCondRatio_Min() {
		return (EAttribute)intCondRatioEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntCondRatio_Max() {
		return (EAttribute)intCondRatioEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTimeUnits() {
		return timeUnitsEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLikelihoods() {
		return likelihoodsEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getConsequences() {
		return consequencesEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralDataTypesFactory getCoralDataTypesFactory() {
		return (CoralDataTypesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		intFrequencyEClass = createEClass(INT_FREQUENCY);
		createEAttribute(intFrequencyEClass, INT_FREQUENCY__MIN);
		createEAttribute(intFrequencyEClass, INT_FREQUENCY__MAX);

		frequencyEClass = createEClass(FREQUENCY);
		createEAttribute(frequencyEClass, FREQUENCY__TIMEUNIT);
		createEAttribute(frequencyEClass, FREQUENCY__LIKELIHOOD);

		exactFrequencyEClass = createEClass(EXACT_FREQUENCY);
		createEAttribute(exactFrequencyEClass, EXACT_FREQUENCY__EXACT);

		exactCondRatioEClass = createEClass(EXACT_COND_RATIO);
		createEAttribute(exactCondRatioEClass, EXACT_COND_RATIO__COND_RATIO);

		intCondRatioEClass = createEClass(INT_COND_RATIO);
		createEAttribute(intCondRatioEClass, INT_COND_RATIO__MIN);
		createEAttribute(intCondRatioEClass, INT_COND_RATIO__MAX);

		// Create enums
		timeUnitsEEnum = createEEnum(TIME_UNITS);
		likelihoodsEEnum = createEEnum(LIKELIHOODS);
		consequencesEEnum = createEEnum(CONSEQUENCES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Add supertypes to classes
		intFrequencyEClass.getESuperTypes().add(this.getFrequency());
		exactFrequencyEClass.getESuperTypes().add(this.getFrequency());

		// Initialize classes and features; add operations and parameters
		initEClass(intFrequencyEClass, IntFrequency.class, "IntFrequency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntFrequency_Min(), theTypesPackage.getReal(), "min", "0.0", 1, 1, IntFrequency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getIntFrequency_Max(), theTypesPackage.getReal(), "max", "0.0", 1, 1, IntFrequency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		EOperation op = addEOperation(intFrequencyEClass, ecorePackage.getEBoolean(), "FrequencyIsPositiveValue", 0, 1);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1);
		addEParameter(op, ecorePackage.getEMap(), "context", 0, 1);

		initEClass(frequencyEClass, Frequency.class, "Frequency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFrequency_Timeunit(), this.getTimeUnits(), "timeunit", null, 1, 1, Frequency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getFrequency_Likelihood(), this.getLikelihoods(), "likelihood", null, 1, 1, Frequency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(exactFrequencyEClass, ExactFrequency.class, "ExactFrequency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExactFrequency_Exact(), theTypesPackage.getReal(), "exact", "0.0", 1, 1, ExactFrequency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = addEOperation(exactFrequencyEClass, ecorePackage.getEBoolean(), "FrequencyIsPositiveValue", 0, 1);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1);
		addEParameter(op, ecorePackage.getEMap(), "context", 0, 1);

		initEClass(exactCondRatioEClass, ExactCondRatio.class, "ExactCondRatio", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExactCondRatio_CondRatio(), theTypesPackage.getReal(), "condRatio", "0.0", 1, 1, ExactCondRatio.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = addEOperation(exactCondRatioEClass, ecorePackage.getEBoolean(), "CondRatioIsPositiveValue", 0, 1);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1);
		addEParameter(op, ecorePackage.getEMap(), "context", 0, 1);

		initEClass(intCondRatioEClass, IntCondRatio.class, "IntCondRatio", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntCondRatio_Min(), theTypesPackage.getReal(), "min", "0.0", 1, 1, IntCondRatio.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getIntCondRatio_Max(), theTypesPackage.getReal(), "max", "0.0", 1, 1, IntCondRatio.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = addEOperation(intCondRatioEClass, ecorePackage.getEBoolean(), "CondRatioIsPositiveValue", 0, 1);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1);
		addEParameter(op, ecorePackage.getEMap(), "context", 0, 1);

		// Initialize enums and add enum literals
		initEEnum(timeUnitsEEnum, TimeUnits.class, "TimeUnits");
		addEEnumLiteral(timeUnitsEEnum, TimeUnits.YEAR_LITERAL);
		addEEnumLiteral(timeUnitsEEnum, TimeUnits.MONTH_LITERAL);
		addEEnumLiteral(timeUnitsEEnum, TimeUnits.WEEK_LITERAL);
		addEEnumLiteral(timeUnitsEEnum, TimeUnits.DAY_LITERAL);
		addEEnumLiteral(timeUnitsEEnum, TimeUnits.HOUR_LITERAL);
		addEEnumLiteral(timeUnitsEEnum, TimeUnits.MINUTE_LITERAL);
		addEEnumLiteral(timeUnitsEEnum, TimeUnits.SECOND_LITERAL);

		initEEnum(likelihoodsEEnum, Likelihoods.class, "Likelihoods");
		addEEnumLiteral(likelihoodsEEnum, Likelihoods.RARE_LITERAL);
		addEEnumLiteral(likelihoodsEEnum, Likelihoods.UNLIKELY_LITERAL);
		addEEnumLiteral(likelihoodsEEnum, Likelihoods.POSSIBLE_LITERAL);
		addEEnumLiteral(likelihoodsEEnum, Likelihoods.LIKELY_LITERAL);
		addEEnumLiteral(likelihoodsEEnum, Likelihoods.CERTAIN_LITERAL);

		initEEnum(consequencesEEnum, Consequences.class, "Consequences");
		addEEnumLiteral(consequencesEEnum, Consequences.INSIGNIFICANT_LITERAL);
		addEEnumLiteral(consequencesEEnum, Consequences.MINOR_LITERAL);
		addEEnumLiteral(consequencesEEnum, Consequences.MODERATE_LITERAL);
		addEEnumLiteral(consequencesEEnum, Consequences.MAJOR_LITERAL);
		addEEnumLiteral(consequencesEEnum, Consequences.CATASTROPHIC_LITERAL);

		// Create resource
		createResource(eNS_URI);
	}

} //CoralDataTypesPackageImpl
