/**
 */
package Coral.CoralDataTypes.impl;

import Coral.CoralDataTypes.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoralDataTypesFactoryImpl extends EFactoryImpl implements CoralDataTypesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CoralDataTypesFactory init() {
		try {
			CoralDataTypesFactory theCoralDataTypesFactory = (CoralDataTypesFactory)EPackage.Registry.INSTANCE.getEFactory(CoralDataTypesPackage.eNS_URI);
			if (theCoralDataTypesFactory != null) {
				return theCoralDataTypesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CoralDataTypesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralDataTypesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CoralDataTypesPackage.INT_FREQUENCY: return createIntFrequency();
			case CoralDataTypesPackage.FREQUENCY: return createFrequency();
			case CoralDataTypesPackage.EXACT_FREQUENCY: return createExactFrequency();
			case CoralDataTypesPackage.EXACT_COND_RATIO: return createExactCondRatio();
			case CoralDataTypesPackage.INT_COND_RATIO: return createIntCondRatio();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case CoralDataTypesPackage.TIME_UNITS:
				return createTimeUnitsFromString(eDataType, initialValue);
			case CoralDataTypesPackage.LIKELIHOODS:
				return createLikelihoodsFromString(eDataType, initialValue);
			case CoralDataTypesPackage.CONSEQUENCES:
				return createConsequencesFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case CoralDataTypesPackage.TIME_UNITS:
				return convertTimeUnitsToString(eDataType, instanceValue);
			case CoralDataTypesPackage.LIKELIHOODS:
				return convertLikelihoodsToString(eDataType, instanceValue);
			case CoralDataTypesPackage.CONSEQUENCES:
				return convertConsequencesToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntFrequency createIntFrequency() {
		IntFrequencyImpl intFrequency = new IntFrequencyImpl();
		return intFrequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Frequency createFrequency() {
		FrequencyImpl frequency = new FrequencyImpl();
		return frequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExactFrequency createExactFrequency() {
		ExactFrequencyImpl exactFrequency = new ExactFrequencyImpl();
		return exactFrequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExactCondRatio createExactCondRatio() {
		ExactCondRatioImpl exactCondRatio = new ExactCondRatioImpl();
		return exactCondRatio;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntCondRatio createIntCondRatio() {
		IntCondRatioImpl intCondRatio = new IntCondRatioImpl();
		return intCondRatio;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeUnits createTimeUnitsFromString(EDataType eDataType, String initialValue) {
		TimeUnits result = TimeUnits.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTimeUnitsToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Likelihoods createLikelihoodsFromString(EDataType eDataType, String initialValue) {
		Likelihoods result = Likelihoods.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLikelihoodsToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Consequences createConsequencesFromString(EDataType eDataType, String initialValue) {
		Consequences result = Consequences.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConsequencesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoralDataTypesPackage getCoralDataTypesPackage() {
		return (CoralDataTypesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static CoralDataTypesPackage getPackage() {
		return CoralDataTypesPackage.eINSTANCE;
	}

} //CoralDataTypesFactoryImpl
