/**
 */
package Coral.CoralDataTypes.impl;

import Coral.CoralDataTypes.CoralDataTypesPackage;
import Coral.CoralDataTypes.ExactCondRatio;

import Coral.CoralDataTypes.util.CoralDataTypesValidator;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exact Cond Ratio</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Coral.CoralDataTypes.impl.ExactCondRatioImpl#getCondRatio <em>Cond Ratio</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExactCondRatioImpl extends MinimalEObjectImpl.Container implements ExactCondRatio {
	/**
	 * The default value of the '{@link #getCondRatio() <em>Cond Ratio</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondRatio()
	 * @generated
	 * @ordered
	 */
	protected static final double COND_RATIO_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCondRatio() <em>Cond Ratio</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondRatio()
	 * @generated
	 * @ordered
	 */
	protected double condRatio = COND_RATIO_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExactCondRatioImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CoralDataTypesPackage.Literals.EXACT_COND_RATIO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCondRatio() {
		return condRatio;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondRatio(double newCondRatio) {
		double oldCondRatio = condRatio;
		condRatio = newCondRatio;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoralDataTypesPackage.EXACT_COND_RATIO__COND_RATIO, oldCondRatio, condRatio));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean CondRatioIsPositiveValue(DiagnosticChain diagnostics, Map context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 CoralDataTypesValidator.DIAGNOSTIC_SOURCE,
						 CoralDataTypesValidator.EXACT_COND_RATIO__COND_RATIO_IS_POSITIVE_VALUE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "CondRatioIsPositiveValue", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoralDataTypesPackage.EXACT_COND_RATIO__COND_RATIO:
				return new Double(getCondRatio());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoralDataTypesPackage.EXACT_COND_RATIO__COND_RATIO:
				setCondRatio(((Double)newValue).doubleValue());
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoralDataTypesPackage.EXACT_COND_RATIO__COND_RATIO:
				setCondRatio(COND_RATIO_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoralDataTypesPackage.EXACT_COND_RATIO__COND_RATIO:
				return condRatio != COND_RATIO_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (condRatio: ");
		result.append(condRatio);
		result.append(')');
		return result.toString();
	}

} //ExactCondRatioImpl
