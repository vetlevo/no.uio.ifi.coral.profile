/**
 */
package Coral.CoralDataTypes.impl;

import Coral.CoralDataTypes.CoralDataTypesPackage;
import Coral.CoralDataTypes.Frequency;
import Coral.CoralDataTypes.Likelihoods;
import Coral.CoralDataTypes.TimeUnits;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Frequency</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Coral.CoralDataTypes.impl.FrequencyImpl#getTimeunit <em>Timeunit</em>}</li>
 *   <li>{@link Coral.CoralDataTypes.impl.FrequencyImpl#getLikelihood <em>Likelihood</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FrequencyImpl extends MinimalEObjectImpl.Container implements Frequency {
	/**
	 * The default value of the '{@link #getTimeunit() <em>Timeunit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeunit()
	 * @generated
	 * @ordered
	 */
	protected static final TimeUnits TIMEUNIT_EDEFAULT = TimeUnits.YEAR_LITERAL;

	/**
	 * The cached value of the '{@link #getTimeunit() <em>Timeunit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeunit()
	 * @generated
	 * @ordered
	 */
	protected TimeUnits timeunit = TIMEUNIT_EDEFAULT;

	/**
	 * The default value of the '{@link #getLikelihood() <em>Likelihood</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLikelihood()
	 * @generated
	 * @ordered
	 */
	protected static final Likelihoods LIKELIHOOD_EDEFAULT = Likelihoods.RARE_LITERAL;

	/**
	 * The cached value of the '{@link #getLikelihood() <em>Likelihood</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLikelihood()
	 * @generated
	 * @ordered
	 */
	protected Likelihoods likelihood = LIKELIHOOD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FrequencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CoralDataTypesPackage.Literals.FREQUENCY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeUnits getTimeunit() {
		return timeunit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeunit(TimeUnits newTimeunit) {
		TimeUnits oldTimeunit = timeunit;
		timeunit = newTimeunit == null ? TIMEUNIT_EDEFAULT : newTimeunit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoralDataTypesPackage.FREQUENCY__TIMEUNIT, oldTimeunit, timeunit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Likelihoods getLikelihood() {
		return likelihood;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLikelihood(Likelihoods newLikelihood) {
		Likelihoods oldLikelihood = likelihood;
		likelihood = newLikelihood == null ? LIKELIHOOD_EDEFAULT : newLikelihood;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoralDataTypesPackage.FREQUENCY__LIKELIHOOD, oldLikelihood, likelihood));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoralDataTypesPackage.FREQUENCY__TIMEUNIT:
				return getTimeunit();
			case CoralDataTypesPackage.FREQUENCY__LIKELIHOOD:
				return getLikelihood();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoralDataTypesPackage.FREQUENCY__TIMEUNIT:
				setTimeunit((TimeUnits)newValue);
				return;
			case CoralDataTypesPackage.FREQUENCY__LIKELIHOOD:
				setLikelihood((Likelihoods)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoralDataTypesPackage.FREQUENCY__TIMEUNIT:
				setTimeunit(TIMEUNIT_EDEFAULT);
				return;
			case CoralDataTypesPackage.FREQUENCY__LIKELIHOOD:
				setLikelihood(LIKELIHOOD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoralDataTypesPackage.FREQUENCY__TIMEUNIT:
				return timeunit != TIMEUNIT_EDEFAULT;
			case CoralDataTypesPackage.FREQUENCY__LIKELIHOOD:
				return likelihood != LIKELIHOOD_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (timeunit: ");
		result.append(timeunit);
		result.append(", likelihood: ");
		result.append(likelihood);
		result.append(')');
		return result.toString();
	}

} //FrequencyImpl
