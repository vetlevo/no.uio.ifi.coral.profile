/**
 */
package Coral.CoralDataTypes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Consequences</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see Coral.CoralDataTypes.CoralDataTypesPackage#getConsequences()
 * @model
 * @generated
 */
public final class Consequences extends AbstractEnumerator {
	/**
	 * The '<em><b>Insignificant</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Insignificant</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INSIGNIFICANT_LITERAL
	 * @model name="Insignificant"
	 * @generated
	 * @ordered
	 */
	public static final int INSIGNIFICANT = 0;

	/**
	 * The '<em><b>Minor</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Minor</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MINOR_LITERAL
	 * @model name="Minor"
	 * @generated
	 * @ordered
	 */
	public static final int MINOR = 1;

	/**
	 * The '<em><b>Moderate</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Moderate</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MODERATE_LITERAL
	 * @model name="Moderate"
	 * @generated
	 * @ordered
	 */
	public static final int MODERATE = 2;

	/**
	 * The '<em><b>Major</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Major</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MAJOR_LITERAL
	 * @model name="Major"
	 * @generated
	 * @ordered
	 */
	public static final int MAJOR = 3;

	/**
	 * The '<em><b>Catastrophic</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Catastrophic</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CATASTROPHIC_LITERAL
	 * @model name="Catastrophic"
	 * @generated
	 * @ordered
	 */
	public static final int CATASTROPHIC = 4;

	/**
	 * The '<em><b>Insignificant</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INSIGNIFICANT
	 * @generated
	 * @ordered
	 */
	public static final Consequences INSIGNIFICANT_LITERAL = new Consequences(INSIGNIFICANT, "Insignificant", "Insignificant");

	/**
	 * The '<em><b>Minor</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINOR
	 * @generated
	 * @ordered
	 */
	public static final Consequences MINOR_LITERAL = new Consequences(MINOR, "Minor", "Minor");

	/**
	 * The '<em><b>Moderate</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MODERATE
	 * @generated
	 * @ordered
	 */
	public static final Consequences MODERATE_LITERAL = new Consequences(MODERATE, "Moderate", "Moderate");

	/**
	 * The '<em><b>Major</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MAJOR
	 * @generated
	 * @ordered
	 */
	public static final Consequences MAJOR_LITERAL = new Consequences(MAJOR, "Major", "Major");

	/**
	 * The '<em><b>Catastrophic</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CATASTROPHIC
	 * @generated
	 * @ordered
	 */
	public static final Consequences CATASTROPHIC_LITERAL = new Consequences(CATASTROPHIC, "Catastrophic", "Catastrophic");

	/**
	 * An array of all the '<em><b>Consequences</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final Consequences[] VALUES_ARRAY =
		new Consequences[] {
			INSIGNIFICANT_LITERAL,
			MINOR_LITERAL,
			MODERATE_LITERAL,
			MAJOR_LITERAL,
			CATASTROPHIC_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Consequences</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Consequences</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Consequences get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Consequences result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Consequences</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Consequences getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Consequences result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Consequences</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Consequences get(int value) {
		switch (value) {
			case INSIGNIFICANT: return INSIGNIFICANT_LITERAL;
			case MINOR: return MINOR_LITERAL;
			case MODERATE: return MODERATE_LITERAL;
			case MAJOR: return MAJOR_LITERAL;
			case CATASTROPHIC: return CATASTROPHIC_LITERAL;
		}
		return null;
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Consequences(int value, String name, String literal) {
		super(value, name, literal);
	}

} //Consequences
