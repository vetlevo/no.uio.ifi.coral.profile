/**
 */
package Coral.CoralDataTypes;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exact Frequency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Coral.CoralDataTypes.ExactFrequency#getExact <em>Exact</em>}</li>
 * </ul>
 *
 * @see Coral.CoralDataTypes.CoralDataTypesPackage#getExactFrequency()
 * @model
 * @generated
 */
public interface ExactFrequency extends Frequency {
	/**
	 * Returns the value of the '<em><b>Exact</b></em>' attribute.
	 * The default value is <code>"0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exact</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exact</em>' attribute.
	 * @see #setExact(double)
	 * @see Coral.CoralDataTypes.CoralDataTypesPackage#getExactFrequency_Exact()
	 * @model default="0.0" dataType="org.eclipse.uml2.types.Real" required="true" ordered="false"
	 * @generated
	 */
	double getExact();

	/**
	 * Sets the value of the '{@link Coral.CoralDataTypes.ExactFrequency#getExact <em>Exact</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exact</em>' attribute.
	 * @see #getExact()
	 * @generated
	 */
	void setExact(double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * self.exact >= 0.0
	 * @param diagnostics The chain of diagnostics to which problems are to be appended.
	 * @param context The cache of context-specific information.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean FrequencyIsPositiveValue(DiagnosticChain diagnostics, Map context);

} // ExactFrequency
